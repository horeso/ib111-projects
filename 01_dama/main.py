"""
Runs simulations of Checkers between specified players
"""
from src.StatsManager import StatsManager
from src.RandomPlayer import RandomPlayer
from src.FirstMovePlayer import FirstMovePlayer
from src.SearchPlayer import SearchPlayer

import time


def main():
    # players = [SearchPlayer('SearchD3Q1', depth=3, queen_value=1, random_moves=True, mode='normal'),
                # SearchPlayer('SearchD1Q2',depth=1, queen_value=2, random_moves=True),
                # RandomPlayer('Random'),
                # FirstMovePlayer('FirstMove'),
                # SearchPlayer('SearchD5Q1', depth=5, queen_value=1,random_moves=False, mode='normal')
                # ]

    # players = [SearchPlayer('SearchD3Q1', depth=3, queen_value=1, random_moves=True, mode='normal'),
               # RandomPlayer('Random'), SearchPlayer('SearchD5Q1', depth=5, queen_value=1, random_moves=True, mode='normal'),
                # FirstMovePlayer('FirstMove'), SearchPlayer('SearchD7Q1', depth=7, queen_value=1, random_moves=False, mode='normal'),
                # SearchPlayer('SearchD9Q1', depth=9, queen_value=1, random_moves=False, mode='normal')]

    players = [SearchPlayer('SearchD5Q1', depth=5, queen_value=1, random_moves=True, mode='adv'),
                SearchPlayer('SearchD5Q3', depth=5, queen_value=3, random_moves=True, mode='adv'),
                SearchPlayer('SearchD5Q5', depth=5, queen_value=5, random_moves=True, mode='adv'),
                SearchPlayer('SearchD5Q7', depth=5, queen_value=7, random_moves=True, mode='adv')]#,
                # SearchPlayer('SearchD7QN', depth=5, queen_value=1, random_moves=True, mode='adv')]

    # players = [FirstMovePlayer('Firstmove'), RandomPlayer('Random')]

    stats_manager = StatsManager(players, games_per_battle=100)
    
    start = time.time()

    stats_manager.generate_stats(threads=8)

    end = time.time()

    print('Simulation took', end - start, 'seconds')
    stats_manager.procces_save_stats()

if __name__ == '__main__':
    main()
