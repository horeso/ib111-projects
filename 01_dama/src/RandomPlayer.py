import random

from src.Player import Player


class RandomPlayer(Player):

    """
    A RandomPlayer selects random legal move
    """

    def __init__(self, strategy_name, number=1):
        Player.__init__(self, strategy_name, number)

    def strategy_func(self, board):
        """
        Inputs:
        - board : Board object representing current state of the board

        Returns:
        - move  
        """

        moves = board.get_possible_moves(self.number)

        random.shuffle(moves)
        if len(moves) == 0:
            return []
        return moves[0]
