import copy

from src.BoardRenderer import BoardRenderer
from src.Board import Board

"""
"""

class Game:

    def __init__(self, player1, player2, board_size, rows):
        """
        Inputs:
        - player1 : Player object
        - player2 : Player object
        - board_size : size of the board
        - rows : how many rows of piece both players have
        """
        self.player1 = player1
        self.player1.set_number(1)
        self.player2 = player2
        self.player2.set_number(2)

        self.board_size = board_size
        self.rows = rows
        self.renderer = BoardRenderer()

    def simulate(self, verbose=False):
        """
        Runs the simulation of one game
        """
        self.board = Board(self.board_size, self.rows)
        run = True
        turn = 0
        while(run):
            if turn % 2 == 0:
                move = self.player1.strategy_func(copy.deepcopy(self.board))

            else:
                move = self.player2.strategy_func(copy.deepcopy(self.board))

            if len(move) == 0 or (self.board.get_player_pieces(1) == 1 and self.board.get_player_pieces(2) == 1):
                return {'player_num': 2 - (turn % 2),
                        'strategy_name': self.player1.strategy_name if turn % 2 == 1
                        else self.player2.strategy_name,
                        'turn': turn}

            self.board.apply_move((turn % 2) + 1, move)

            # if verbose:
            #     print(self.renderer.to_string(self.board.positions))
            #     print()

            turn += 1
            if turn >= 300:
                print('Lenght of game is over 300')
                if self.board.get_player_pieces(1) > self.board.get_player_pieces(2):
                    return {'player_num': 1,
                            'strategy_name': self.player1.strategy_name,
                            'turn': turn}
                else:
                    return {'player_num': 2,
                            'strategy_name': self.player2.strategy_name,
                            'turn': turn}
