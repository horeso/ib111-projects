import copy
import random

from src.Player import Player

"""
Strategy which does depth search 

We can choose between several heuristic scoring functions.
We can alter the value of queen and also decide, whether we
want to choose randomly if we have more moves with the same value.
"""

class SearchPlayer(Player):

    def __init__(self, strategy_name, depth, mode='normal', queen_value=1, random_moves=True, number=1):
        Player.__init__(self, strategy_name, number)
        self.depth = depth
        self.queen_value = queen_value
        self.random_moves = random_moves

        if mode == 'normal':
            self.score_func = self.get_score_normal
        elif mode == 'offensive':
            self.score_func = self.get_score_offensive
        elif mode == 'defensive':
            self.score_func = self.get_score_defensive
        elif mode == 'adv':
            self.score_func = self.get_score_adv
        else:
            print("setting score function to default normal")
            self.score_func = self.get_score_normal

    def strategy_func(self, board):

        self.board = board
        self.next_moves = []
        self.next_vals = []
        val, move = self.depth_search2(self.depth, -1000, 1000, True)

        if len(self.next_vals) == 0:
            return []

        max_val = max(self.next_vals)
        max_moves = []
        for i in range(len(self.next_moves)):
            if self.next_vals[i] == max_val:
                max_moves.append(self.next_moves[i])
            
        if not self.random_moves:
            return max_moves[0]

        random.shuffle(max_moves)

        return max_moves[0]

    def depth_search2(self, cur_depth, alfa, beta, maxim_player):
        """
        Depth search with alfa beta pruning
        """
        if cur_depth <= 0:
            return (self.score_func(self.board), [])

        if maxim_player:
            val = -1000
            best_move = []
            for move in self.board.get_possible_moves(self.number):
                self.board.apply_move(self.number, move)
                new_val = self.depth_search2(cur_depth - 1, alfa, beta, False)[0]
                if new_val >= val:
                    val = new_val
                    best_move = move

                    if cur_depth == self.depth:
                        self.next_moves.append(move)
                        self.next_vals.append(val)

                self.board.reverse_move(self.number, move)

                alfa = max(alfa, val)
                if beta <= alfa:
                    break

            return (val, best_move)

        else:
            val = 1000
            best_move = []
            number = 3 - self.number
            for move in self.board.get_possible_moves(number):
                self.board.apply_move(number, move)
                new_val = self.depth_search2(cur_depth - 1, alfa, beta, True)[0]
                if new_val <= val:
                    val = new_val
                    best_move = move

                self.board.reverse_move(number, move)

                beta = min(beta, val)
                if beta <= alfa:
                    break

            return (val, best_move)

    def get_score_normal(self, board):
        """
        Normal piece: val = 1
        Queen: val = queen_value
        """
        my_normal, my_queens, o_normal, o_queens = board.get_counts(self.number)
        score = my_queens*self.queen_value - o_queens*self.queen_value + my_normal - o_normal
        
        # last piece of enemy must be destroyed whatever cost
        if o_normal + o_queens == 0 and my_normal > 0:
            return 200

        return score

    def get_score_offensive(self, board):
        """
        Just minimizes enemy pieces, does not count with own pieces
        """
        this_normal, this_queens, other_normal, other_queens = board.get_counts(self.number)

        score = -other_queens*self.queen_value - other_normal 

        # last piece of enemy must be destroyed whatever cost
        if other_normal + other_queens == 0 and this_normal > 0:
            return 200
        return score

    def get_score_defensive(self, board):
        """
        Just maximizes own pieces, does not care about enemy
        """
        this_normal, this_queens, other_normal, other_queens = board.get_counts(self.number)
    
        score = this_queens*self.queen_value + this_normal        

        # last piece of enemy must be destroyed whatever cost
        if other_normal + other_queens == 0 and this_normal > 0:
            return 200

        return score

    def get_score_adv(self, board):
        """
        Normal score + heuristics

        - if we have piece that can promote in next move: + 0.1
        - if we have piece in edge column: + 0.3
        """
        score = self.get_score_normal(board)

        my_pos = board.get_player_positions(self.number)

        other_pos = board.get_player_positions(3 - self.number)


        for pos in my_pos:
            # is_safe
            if pos[1] == board.size-1 or pos[1] == 0:
                score += 0.3
            # is_attacking
            if pos[0] > board.size-3:
                score += 0.1

        for pos in other_pos:
            # is_safe
            if pos[1] == board.size-1 or pos[1] == 0:
                score -= 0.3
            # is_attacking
            if pos[0] < 2:
                score -= 0.1

        return score
