
"""
Converting Board object into string
"""
class BoardRenderer:

    """
    BoardRenderer converts list of positions into a string which
    looks like a normal board
    """

    def __init__(self):
        pass

    def to_string(self, positions, size=8, p1_symbol='x', p2_symbol='o', empty_symbol='.'):
        """
        Inputs:
        - positions:
        - size:
        - p1_symbol:
        - p2_symbol:
        - empty_symbol:

        Returns a string representing the state of the game
        """

        board_string = ''
        p1_queen_symbol = p1_symbol.upper()
        p2_queen_symbol = p2_symbol.upper()

        symbols = {0: empty_symbol, 1: p1_symbol, 2: p2_symbol,
                   11: p1_queen_symbol, 22: p2_queen_symbol}

        board = [empty_symbol * size] * size

        for position in positions:
            row = position[0]
            col = position[1]
            piece = symbols[position[2]]

            s = board[row]
            new_col = s[:col] + piece + s[col+1:]

            board[row] = new_col

        board = '\n'.join(board)

        return board
