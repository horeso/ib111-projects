"""
Processes output of simulations,
generates ranking table, also statistics about simulation

Used to create pie charts, but that was removed,
because pie charts are evil
"""

from pathos.multiprocessing import Pool 
import copy
import matplotlib.pyplot as plt
import plotly.plotly as py

from src.Game import Game


class StatsManager:

    def __init__(self, players, games_per_battle=100000, directory='stats'):
        self.games_per_battle = games_per_battle
        self.players = players
        self.dir = directory
        self.dir = self.dir + '/'
        
    def generate_stats(self, threads=6):
        """
        
        """
        self.statistics = []
        for first_index in range(len(self.players)):
            for second_index in range(len(self.players)):
                player1 = copy.deepcopy(self.players[first_index])
                player1.set_number(1)
                player2 = copy.deepcopy(self.players[second_index])
                player2.set_number(2)

                print("simulating: " + player1.strategy_name 
                    + ' vs ' + player2.strategy_name)
                
                game = Game(player1, player2, 8, 3)
                
                # multi thread version
                pool = Pool(threads)
                stats = pool.map(game.simulate, range(self.games_per_battle))
                
                # # one core version
                # stats = []
                # for i in range(self.games_per_battle):
                    # stats.append(game.simulate())
                
                average_length = sum([x['turn'] for x in stats]) / float(len(stats))
                           
                p1_wins = [x['player_num'] for x in stats].count(1)
                p2_wins = [x['player_num'] for x in stats].count(2)
                           
                self.statistics.append({'strategy1': player1.strategy_name,
                                       'strategy2:': player2.strategy_name,
                                       'average_length': average_length,
                                       'strategy1_wins': p1_wins,
                                       'strategy2_wins': p2_wins})
                
    def procces_save_stats(self):
        # [wins, loses, draws, games_won, total_games_played]
        total_results = {player.strategy_name: [0,0,0,0,0] for player in self.players}

        average_length = 0

        for result in self.statistics:
            wins1 = result['strategy1_wins']
            wins2 = result['strategy2_wins']

            # we do not count if same strategy plays with itself
            if result['strategy1'] != result['strategy2:']:
                total_results[result['strategy1']][3] += wins1
                total_results[result['strategy2:']][3] += wins2
                total_results[result['strategy1']][4] += wins1 + wins2
                total_results[result['strategy2:']][4] += wins1 + wins2

                # Strategy 1 wins
                if wins1 > wins2:
                    total_results[result['strategy1']][0] += 1
                    total_results[result['strategy2:']][1] += 1

                # Strategy 2 wins
                elif wins1 < wins2:
                    total_results[result['strategy1']][1] += 1
                    total_results[result['strategy2:']][0] += 1
                # Draw
                else:
                    total_results[result['strategy1']][2] += 1
                    total_results[result['strategy2:']][2] += 1

            average_length += result['average_length']

        average_length /= len(self.statistics)
        
        print(total_results)

        self.create_save_ranking(copy.deepcopy(total_results))
        self.create_save_table(copy.deepcopy(total_results))
        self.create_save_statistics(average_length, self.games_per_battle, len(self.players))
    
    def create_save_ranking(self, total_results):
        """
        Creates ranking table
        """
        f = open(self.dir+'ranking.csv', 'w')
        f.write('Strategy,Wins,Loss,Draw,Score\n')

        while(len(total_results.keys()) > 0):
            max_dif = -50
            max_key = ""
            for key, item in total_results.items():
                dif = total_results[key][0] * 2 + total_results[key][2]
                if dif > max_dif:
                    max_dif = dif
                    max_key = key

                # if they have same wins, but different score
                elif dif == max_dif and total_results[key][3] > total_results[max_key][3]:
                    max_key = key

            max_list = total_results[max_key]
            to_write = max_key + ',' + str(max_list[0]) + ',' + str(max_list[1]) \
                        + ',' + str(max_list[2]) + ',' \
                        + str(max_list[3] / max_list[4] * 100.0)+'%'
            
            f.write(to_write + '\n')

            del total_results[max_key]

        f.close()
        
    def create_save_table(self, total_results):
        """
        Creates table of percentage wins,
        starting players are in rows, second players are in columns,
        Shows win ratio of starting players.
        """
        f = open(self.dir+'/table.csv', 'w')

        header = ''
        index = []
        for key in total_results.keys():
            header += ',' + key
            index.append(key)

        f.write(header + '\n')

        rows = [[0 for j in range(len(total_results.keys()))] 
                    for i in range(len(total_results.keys()))]

        # prepares result 2D-list
        for result in self.statistics:
            wins1 = result['strategy1_wins']
            wins2 = result['strategy2_wins']
            w1_to_w2 = wins1 / (wins1 + wins2) * 100

            row = index.index(result['strategy1'])
            col = index.index(result['strategy2:'])
            rows[row][col] = w1_to_w2

        # writes to file
        for i in range(len(index)):
            to_write = index[i]
            for j in range(len(rows[0])):
                to_write += ',' + str(round(rows[i][j],4)) + ' %'
            f.write(to_write + '\n')

        f.close()
        
    def create_save_statistics(self, average_length, games_per_battle, players_count):
        """
        Saves statistics about the simulation
        """
        f = open(self.dir+'statistics.csv', 'w')
        f.write('average length,' + str(average_length) + '\n')
        f.write('total games played,' + 
                    str(games_per_battle * players_count**2) + '\n')
        f.write('number of players,' + str(players_count) + '\n')
        f.write('games per battle,' + str(games_per_battle) + '\n')
        f.close()
