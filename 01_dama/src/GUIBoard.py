import tkinter as tk
import copy

from src.Game import Game
from src.Board import Board
from src.SearchPlayer import SearchPlayer

"""
Creates playable GUI interface for this game
"""


class GUIBoard(tk.Frame):

    def __init__(self, parent, player, rows=8, cols=8, size=64):
        tk.Frame.__init__(self, parent)

        self.board = Board()

        self.player = player

        self.rows = rows
        self.columns = cols
        self.size = size
        self.color1 = 'white'
        self.color2 = 'white'

        self.all_moves = []

        canvas_width = self.columns * size
        canvas_height = rows * size

        self.canvas = tk.Canvas(self, borderwidth=0, highlightthickness=0,
                                width=canvas_width, height=canvas_height, background="grey")
        self.canvas.pack(side="top", fill="both", expand=True, padx=0, pady=0)

        self.canvas.bind('<Button-1>', self.process_click)
        self.bind('<Left>', self.reverse_one_move)
        self.bind('<Up>', self.reverse_all_moves)
        self.focus_set()

        self.pack(side="top", fill="both", expand="true", padx=4, pady=4)

        self.turn = 0
        self.choosing = 0
        self.last_choosing_from = (0, 0)
        self.possible_coords = []
        self.last_possible_moves = []
        self.refresh()
        self.draw_game(self.board.positions)

    def draw_cross(self, row, col):
        """
        Draws cross over whole cell

        Inputs:
        - row : row index
        - col : col index
        - color : string
        """
        self.canvas.create_line(col*self.size, row*self.size, (col+1)*self.size,
                                (row+1)*self.size, width=5, fill='red')

        self.canvas.create_line((col+1)*self.size-col, row*self.size-row, col*self.size-col,
                                (row+1)*self.size-row, width=5, fill='red')

    def draw_circle(self, row, col, color):
        """
        Draws circle over whole cell

        Inputs:
        - row : row index
        - col : col index
        - color : string
        """
        self.canvas.create_oval(col*self.size, row*self.size, (col+1)*self.size,
                                (row+1)*self.size, width=5, fill=color, outline=color)

    def draw_rectangle(self, row, col, color):
        """
        Draws rectangle over whole cell

        Inputs:
        - row : row index
        - col : col index
        - color : string
        """
        x1 = (col * self.size)
        y1 = (row * self.size)
        x2 = x1 + self.size
        y2 = y1 + self.size
        self.canvas.create_rectangle(x1, y1, x2, y2, fill=color)

    def draw_small_rectangle(self, row, col, color):
        """
        Draws smaller rectangle, used for queen representation

        Inputs:
        - row : row index
        - col : col index
        - color : string
        """
        x1 = (col * self.size) + self.size/4
        y1 = (row * self.size) + self.size/4
        x2 = x1 + self.size - self.size/2
        y2 = y1 + self.size - self.size/2
        self.canvas.create_rectangle(x1, y1, x2, y2, fill=color)

    def draw_game(self, positions):
        """
        Draws pieces on canvas

        Inputs:
        - positions : list of positions
        """

        for position in positions:
            row = position[0]
            col = position[1]
            item = position[2]

            if item == 1:
                self.draw_circle(row, col, 'red')
            elif item == 2:
                self.draw_circle(row, col, 'black')
            elif item == 11:
                self.draw_circle(row, col, 'red')
                self.draw_small_rectangle(row, col, 'black')
            elif item == 22:
                self.draw_circle(row, col, 'black')
                self.draw_small_rectangle(row, col, 'red')

    def process_click(self, event):
        """

        """
        x = event.x
        y = event.y
        row = y // self.size
        col = x // self.size

        if row >= self.board.size or col >= self.board.size:
            return

        on_turn = (self.turn % 2) + 1

        # we selected some piece
        if self.choosing == 1:
            if (row, col) in self.possible_coords:

                chosen_move = []
                for move in self.last_possible_moves:
                    first = move[0]
                    if first[0] == self.last_choosing_from[0] and first[1] == self.last_choosing_from[1] \
                            and first[2] == row and first[3] == col:

                        print('my move:', move)
                        self.board.apply_move(on_turn, move)
                        self.all_moves.append(move)
                        self.turn += 2

                        other_move = self.player.strategy_func(
                            copy.deepcopy(self.board))

                        print('AI move: ', other_move)
                        self.board.apply_move(2, other_move)
                        self.all_moves.append(other_move)

                        self.refresh()
                        self.draw_game(self.board.positions)

            self.choosing = 0

        # if we have not any piece selected
        if self.choosing == 0:
            self.possible_coords = []
            self.last_possible_moves = []
            self.refresh()
            if self.board.find_by_position(row, col)[2] in [on_turn, on_turn*11]:

                self.last_choosing_from = (row, col)
                self.draw_rectangle(row, col, 'lightblue')
                moves = self.board.get_possible_moves((self.turn % 2)+1)
                self.last_possible_moves = moves
                for move in moves:
                    #                     print move
                    first = move[0]
                    if first[0] == self.last_choosing_from[0] and first[1] == self.last_choosing_from[1]:
                        self.possible_coords.append((first[2], first[3]))
#                         print first[0], first[1]
                        self.draw_rectangle(first[2], first[3], 'blue')
                        self.choosing = 1

            self.draw_game(self.board.positions)

    def refresh(self):
        """
        Redraws canvas and gameboard without pieces
        """
        self.canvas.delete("all")

        color = self.color2
        for row in range(self.rows):
            for col in range(self.columns):
                x1 = (col * self.size)
                y1 = (row * self.size)
                x2 = x1 + self.size
                y2 = y1 + self.size
                self.canvas.create_rectangle(
                    x1, y1, x2, y2, outline="black", fill=color)

    def reverse_all_moves(self, event):
        """

        """
        self.all_moves.reverse()

        length = len(self.all_moves)

        player_number = 2 - length % 2

        for move in self.all_moves:
            print("reverting:", move)
            self.board.reverse_move(player_number, move)
            player_number = 3 - player_number

        print("move reversed", length)

        self.refresh()
        self.draw_game(self.board.positions)

        self.all_moves = []

    def reverse_one_move(self, event):
        """

        """

        self.board.reverse_move(2, self.all_moves.pop())
        self.board.reverse_move(1, self.all_moves.pop())

        self.refresh()
        self.draw_game(self.board.positions)
