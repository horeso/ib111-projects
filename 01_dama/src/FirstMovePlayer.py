from src.Player import Player

"""
This strategy returns first move it can do.
"""

class FirstMovePlayer(Player):

    def __init__(self, strategy_name, number=1):
        Player.__init__(self, strategy_name, number)

    def strategy_func(self, board):
        moves = board.get_possible_moves(self.number)

        if len(moves) == 0:
            return []
        return moves[0]
