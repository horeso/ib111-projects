from src.BoardRenderer import BoardRenderer

"""
Board class represents the game board.
"""

class Board:

    def __init__(self, size=8, rows=3):

        # list of (row, col, piece, player_num) ... piece normal/ queens
        # (player_number / 11*player_number) ... player_num (1 / 2)
        self.positions = []
        self.size = size
        self.rows = rows

        self.prepare_board(size, rows)
       
    # prepares board with dimensions size x size, n rows of pices for
    # each player
    def prepare_board(self, size, rows):
        """
        Prepares the game board, for example size=6, rows=2

        X.X.X.
        .X.X.X
        ......
        Y.Y.Y.
        .Y.Y.Y

        Inputs:
        - size : defines dimensions of the board, size x size
        - rows : specifies how many row of pieces every player has
        """

        if size <= 2 * rows:
            raise ValueError("Cannot create this board, size={}, rows={}".format(size, rows))

        self.size = size
        self.positions = []
        for row in range(size):
            row_list = []
            for col in range(size):

                if (row+col) % 2 == 0:
                    if row < rows:
                        self.positions.append((row, col, 1, 1))
                    elif row >= size - rows:
                        self.positions.append((row, col, 2, 2))
    
    def get_player_positions(self, player_number):
        """
        Inputs:
        - player_number : We want positions of this player, val 1 or 2

        Returns list of positions
        """
        return [position for position in self.positions if position[3] == player_number]

    def find_by_position(self, row, col):
        """
        Finds if there is a piece on position specified by 'row' and 'col'.

        Inputs:
        - row : row index 
        - col : column index

        Returns position of piece if found, else returns (-1, -1, -1, -1)
        """
        for position in self.positions:
            if position[0] == row and position[1] == col:
                return position

        # position not found
        return (-1, -1, -1, -1)    

    def get_capture_move_normal(self, row, col, player_number, enemy_pieces):
        """
		Finds all possible capturing moves for a normal of given player
        Inputs:
        - row: row where piece is
        - col: column where piece is
        - player_number: number of player 1 or 2
        - enemy_pieces: list of enemy pieces
        """
        new_moves = []
        
        def new_row(row, amount, player_number):
            if player_number == 1:
                return row + amount
            else:
                return row - amount

        for d in [-1, 1]:
            
            next_row = new_row(row, 1, player_number)
            if self.is_inboard(next_row, col + d) and  \
                self.is_piece(next_row, col + d, enemy_pieces) \
                and self.is_empty(new_row(row, 2, player_number), col + 2*d):
                
                captured_piece = self.find_by_position(next_row, col + d)[2]
                is_promotion = self.is_promoting(player_number, new_row(row, 2, player_number))

                to_append = [(row, col, new_row(row, 2, player_number),
                             col + 2*d, captured_piece, player_number, is_promotion)]
                # recursively calls itself
                next_moves = self.get_capture_move_normal(
                        new_row(row, 2, player_number), col + 2*d, player_number, enemy_pieces)
                
                if len(next_moves) == 0:
                    new_moves.append(to_append)
                else:
                    for next_move in next_moves:
                        new_moves.append(to_append+next_move)
                
        
        return new_moves
        
            
    def get_capture_move_queen(self, row, col, player_number, enemy_pieces, captured_positions):
        """

        Inputs:
        - row :
        - col :
        - player_number : 
        - enemy_pieces :
        """
        new_moves = []
        
        for row_dir in [-1, 1]:
            for col_dir in [-1, 1]:
                cur_row = row + row_dir
                cur_col = col + col_dir
                # queen can move more spaces in one turn
                while(self.is_empty(cur_row, cur_col)):
                    cur_row += row_dir
                    cur_col += col_dir
                    
                cur_row -= row_dir
                cur_col -= col_dir
            
                # checks if it has not captured already on that position
                if (cur_row+row_dir, cur_col+col_dir) not in captured_positions \
                    and self.is_piece(cur_row+row_dir, cur_col+col_dir, enemy_pieces) \
                    and self.is_empty(cur_row+2*row_dir, cur_col+2*col_dir):

                    new_row = cur_row + 2*row_dir
                    new_col = cur_col + 2*col_dir

                    captured_piece = self.find_by_position(cur_row + row_dir, cur_col + col_dir)[2]

                    to_append = [(row, col, new_row, new_col, captured_piece, player_number*11, False)]

                    temp_capt_positions = captured_positions + [(new_row - row_dir, new_col - col_dir)]
                    
                    next_moves = self.get_capture_move_queen(new_row, new_col,
                                    player_number, enemy_pieces, temp_capt_positions)
                    
                    if len(next_moves) == 0:
                        new_moves.append(to_append)
                    else:
                        for next_move in next_moves:
                            new_moves.append(to_append + next_move)
        
        return new_moves
        
        
    def get_move_normal(self, row, col, player_number):
        """

        Inputs:
        - row :
        - col :
        - player_number : 
        - enemy_pieces :
        """
        possible_moves = []
        
        new_row = row + 1 if player_number == 1 else row - 1

        for d in [-1, 1]:
            new_col = col + d
            
            if self.is_empty(new_row, new_col):
                is_promotion = self.is_promoting(player_number, new_row)
                possible_moves.append(
                    [(row, col, new_row, new_col, 0, player_number, is_promotion)])
                
        return possible_moves
            
    def get_move_queen(self, row, col, player_number):
        """

        Inputs:
        - row :
        - col :
        - player_number : 
        - enemy_pieces :
        """
        possible_moves = []
        
        # go to all 4 directions
        for row_dir in [-1, 1]:
            for col_dir in [-1, 1]:
                next_row = row
                next_col = col
                while(self.is_empty(next_row + row_dir, next_col + col_dir)):
                    next_row += row_dir
                    next_col += col_dir
                    possible_moves.append(
                        [(row, col, next_row, next_col, 0, player_number*11, False)])
                    
        return possible_moves
    
    def get_possible_moves(self, player_number):
        """
        Finds legal moves for given player. If player can capture
        something, then he has to. Also queen has to capture before
        other normal pieces.

        Inputs:
        - player_number: Number of the player on turn, value 1 or 2

        Returns:
        - possible_moves: list of possible moves
        """
        possible_moves = []
        queen_moves = []
        positions = self.get_player_positions(player_number)
        # print('positions of pl', player_number, positions)
        
        # 1-0=1 1-1=0, here we translate: 1-(n-1)+1=3-n, n=1,2
        # we get numbers of other player
        enemy_pieces = [(3 - player_number), 11 * (3 - player_number)]
         
        # capturing moves first
        for position in positions:
            row = position[0]
            col = position[1]
            piece = position[2]
            
            # normal piece
            if piece % 11 != 0:
                possible_moves.extend(self.get_capture_move_normal(
                            row, col, player_number, enemy_pieces))
            # queen
            else:
                queen_moves.extend(self.get_capture_move_queen(
                            row, col, player_number, enemy_pieces, []))
        
        # if we can capture with queen, we must to
        if len(queen_moves) > 0:
            return queen_moves
        
        # if we can capture with other piece, we must to
        elif len(possible_moves) > 0:
            return possible_moves
        
        # non-capturing moves
        for position in positions:
            row = position[0]
            col = position[1]
            piece = position[2]
            
            # normal piece
            if piece % 11 != 0:
                possible_moves.extend(self.get_move_normal(
                            row, col, player_number))
            #queen
            else:
                possible_moves.extend(self.get_move_queen(
                            row, col, player_number))
                
        return possible_moves
        
    
    # applies move to board
    def apply_move(self, player_number, move):
        """
        Applies move to the board, if given ilegal move
        prints which error occured

        Inputs:
        - player_number : Number of the player on turn, 1 or 2
        - move : list of tuples describing the move
        """
        for elem_move in move:
            row1, col1, row2, col2, captures, who_captures, is_promotion = elem_move
            symbol = who_captures
            
            # case when we want to move a piece but it is different type
            if (row1, col1, who_captures, player_number) not in self.positions:
                who_captures = player_number if who_captures % 11 == 0 else player_number * 11
            self.positions.remove((row1, col1, who_captures, player_number))

            if captures != 0:
                
                # direction of movement
                row_dir = (row2 - row1) // abs(row2 - row1)
                col_dir = (col2 - col1) // abs(col2 - col1)                
                captured_row = row2 - row_dir
                captured_col = col2 - col_dir

                # if we want to capture a piece, but it is different type
                if (captured_row, captured_col, captures, 3 - player_number) not in self.positions:
                    captures = 3 - player_number if captures % 11 == 0 else (3 - player_number)*11

                self.positions.remove((captured_row, captured_col, captures, 3 - player_number))
                
            # piece changes to queen, p1 queen: 11, p2 queen: 22
            if row2 == self.size - 1 and player_number == 1:
                symbol = 11

            elif row2 == 0 and player_number == 2:
                symbol = 22

            self.positions.append((row2, col2, symbol, player_number))

    # unapplies move
    def reverse_move(self, player_number, move):
        """
        Unapplies the move

        Inputs:
        - player_number : Number of the player on turn, 1 or 2
        - move : list of tuples describing the move   
        """
        length = len(move)
        for i in range(length):
            row1, col1, row2, col2, captured, capturing, is_promotion = move[length-i-1]

            capturing_before = capturing
            capturing_after = capturing

            if (row2, col2, capturing, player_number) not in self.positions:
                capturing_after = player_number*11

            # undo the promotion of piece
            if is_promotion:
                capturing_before = player_number

            self.positions.remove((row2, col2, capturing_after, player_number))
            self.positions.append((row1, col1, capturing_before, player_number))

            if captured != 0:

                row_dir = (row2 - row1) // abs(row2 - row1)
                col_dir = (col2 - col1) // abs(col2 - col1)

                self.positions.append((row2 - row_dir, col2 - col_dir, captured, 3 - player_number))

    def is_inboard(self,row,col):
        """
        Checks if 'row' and 'col' are legal indices for this board

        Inputs:
        - row : row index
        - col : col index

        Returns True if indexing is legal
        """
        return (row < self.size and row >= 0 and col < self.size and col >= 0)
    
    def is_empty(self,row,col):
        """
        Checks if position defined by 'row' and 'col' is empty
        
        Inputs:
        - row : row index
        - col : col index

        Returns True if the position is empty
        """
        return (self.is_inboard(row,col) and self.find_by_position(row, col)[0] == -1)

    def is_piece(self, row, col, pieces_list):
        """
        Checks if piece specified by 'pieces_list'
        
        Inputs:
        - row : row index
        - col : col index

        Returns True if the position is empty
        """
        return (self.is_inboard(row, col) and self.find_by_position(row, col)[2] in pieces_list)
    
    def get_player_pieces(self, player_number):
        """
        Inputs:
        - player_number : number of player we want information about

        Returns number of all pieces of given player
        """
        return len([1 for position in self.positions if position[3] == player_number])
    
    def get_player_queens(self, player_number):
        """
        Inputs:
        - player_number : number of player we want information about

        Returns number of queens of given player
        """
        return len([1 for position in self.positions if position[2] == player_number*11 and position[3] == player_number])

    def is_promoting(self, player_number, row):
        """
        Checks if the piece is promoting

        Input:
        - player_number : Number of player on turn, 1 or 2
        - row : row index

        Returns True if piece is in last or first row -> is promoting 
        """
        if player_number == 1 and row == self.rows - 1:
            # print(player_number, row)
            return True
        elif player_number == 2 and row == 0:
            # print(player_number, row)
            return True

        return False

    def get_counts(self, player_number):
        """
    
        Inputs:
        - player_number : Number of player on turn, 1 or 2

        Returns tuple of counts (normal_piece, number_queens, other_player_normal, other_player_queens)
        """
        this_normal = 0
        this_queens = 0
        other_normal = 0
        other_queens = 0

        for position in self.positions:
            if player_number == position[3]:
                if player_number == position[2]:
                    this_normal += 1
                else:
                    this_queens += 1
            else:
                if 3 - player_number == position[2]:
                    other_normal += 1
                else:
                    other_queens += 1

        return (this_normal, this_queens, other_normal, other_queens)