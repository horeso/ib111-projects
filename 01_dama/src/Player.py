
"""
Base Player class, abstract
Every child should has to define strategy_func
"""

class Player:

    def __init__(self, strategy_name, number=1):
        self.strategy_name = strategy_name

        # 1 or 2
        self.number = number

    def strategy_func(self, board):
        """
        Inputs:
        - board : Board object representing current state of the board

        Returns 
        """
        print("this is base class, you should implement this in children")

    def set_number(self, number):
        """
        Sets player number. Has to be 1 or 2.
        """
        if number not in [1, 2]:
            raise ValueError("Player cannot have number {}.".format(number))
        self.number = number
