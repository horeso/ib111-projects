from src.Game import Game
from src.SearchPlayer import SearchPlayer
from src.RandomPlayer import RandomPlayer
from src.BoardRenderer import BoardRenderer

# simulates one game

results = []
# game = Game(SearchPlayer('search1',5,1,number=1),SearchPlayer('search2',3,1,number=2), 8, 3)
game = Game(SearchPlayer('searchD9Q1', depth=7, mode='normal', queen_value=1, random_moves=True, number=1),
            SearchPlayer('searchD5Q1S2', depth=7, mode='normal', queen_value=3, random_moves=True, number=2), 8, 3)
# game = Game(RandomPlayer('random',number=1),
# SearchPlayer('searchD5Q1', depth=5, mode='normal', queen_value=1,random_moves=True,number=2), 8, 3)

games = 10
for i in range(games):
    result = game.simulate(verbose=True)
    results.append(result)
#     if i % 10 == 0:
    print(i / float(games)*100, '%')

p1_wins = [x['player_num'] for x in results].count(1)
p2_wins = [x['player_num'] for x in results].count(2)
print(p1_wins)
print(p2_wins)
# print(results)
