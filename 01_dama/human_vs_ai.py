"""
Starts a new game, where you can play againts AI.
"""

import sys
import tkinter as tk

from src.GUIBoard import GUIBoard
from src.SearchPlayer import SearchPlayer


if len(sys.argv) > 1:
	print('AI is a search strategy with depth', sys.argv[1])
	depth = int(sys.argv[1])
else:
	depth = 5
	print('Using default depth for AI search: ', depth)
player = SearchPlayer('search', depth=depth, queen_value=3,
                      random_moves=False, number=2, mode='adv')

root = tk.Tk()

root.wm_resizable(0, 0)
game_board = GUIBoard(root, player)
root.mainloop()
