"""
Creates visualisations, works with numpy arrays and returns
numpy arrays
"""
import numpy as np

from src.imgutils import read_image

class ImageCreator:
    
    def __init__(self):
        self.digit_imgs = self.load_digit_imgs()
    
    #
    
    def create_img_with_empties(self, img, empty):
        """
        Creates image with X char on empty cells.

        Inputs:
        - img: numpy array to draw on
        - empty: list of indices of empty cells

        Returns np array representing new image
        """
        w, h = img.shape
        empty_img = np.zeros((w, h, 3))
        # original imge is red 
        empty_img[:,:,0] = np.copy(img)
        for i in empty:
            row = i // 9
            col = i % 9

            self.draw_digit(empty_img, row * 64, col * 64, 1, 1)

        return empty_img

    def create_img_with_digits(self, img, indices, digits, rgb=True):
        """
        Creates image with drawn recognized digits

        Inputs:
        - img: numpy array to draw on
        - indices: list of indices of empty cells
        - digits: list of digits coresponding to 'indices'
        - rgb: specifies if 'img' has depth 3 or not 
        """
        if rgb:
            new_img = np.copy(img)
        else:
            h, w = img.shape
            new_img = np.zeros((h, w, 3))
            new_img[:,:,0] = np.copy(img)

        for i in range(len(indices)):
            ind = indices[i]
            row = ind // 9
            col = ind % 9

            self.draw_digit(new_img, row * 64, col * 64, digits[i] + 1, 1)

        return new_img

    def load_digit_imgs(self, size=64):
        """
        Loads digit images from files to memory.
        These images are used to draw them on sudoku image for visualisation.

        digits 1 - 9 + 'X' char

        Inputs:
        - size: resolution of digit array

        Returns:
        - digit_imgs: array of shape (10, size, size)
        """
        digit_imgs = np.zeros((10, size, size))
        for i in range(1, 10):
            img, shape = read_image('data/drawing_digits/'+str(i)+'.png', size)
            digit_imgs[i, :, :] = 1 - img
        x, _ = read_image('data/drawing_digits/X.png', size)
        digit_imgs[0, :, :] = 1 - x

        return digit_imgs

    def draw_digit(self, img, row_pix, col_pix, digit, color):
        """
        Draws one digit to the image

        Inputs:
        - img: image to draw on (size, size, 3)
        - row_pix: starting row pixel for drawing
        - col_pix: starting column pixel
        - digit: number specifing digit we want
        - color: to which channel to draw to

        Returns image with new digit drawn on it
        """
        digit_img = self.digit_imgs[digit - 1]
        H, W = digit_img.shape
        img[row_pix: row_pix + H, col_pix: col_pix + W, color] = digit_img

    # returns image with contour line and dots drawn on it
    def create_contour_img(self, img, contour, dot_positions):
        """
        Creates image with contour line and highlighted dots
        This is used for visualisation of found border of sudoku
        image

        Inputs:
        - img: image to draw on
        - contour: list of contour positions
        - dot_positions: positions of dots

        Returns image with contour and dots
        """
        h, w = img.shape
        out_img = np.zeros((h, w, 3))
        out_img[:,:,0] = np.copy(img)

        self.draw_line(out_img, contour, 1)

        for dot in dot_positions:
            self.draw_dot(out_img, dot, 1)

        return out_img
    
    # draws line on image with some width
    def draw_line(self, contour_img, contour, color):
        """
        """
        for con in contour:
            # This makes the line more wide, so it can be seen
            contour_img[con[1], con[0], 1] = 1
            contour_img[con[1] + 1, con[0], 1] = 1
            contour_img[con[1] - 1, con[0], 1] = 1
            contour_img[con[1], con[0] + 1, 1] = 1
            contour_img[con[1], con[0] - 1, 1] = 1

    def draw_dot(self, img, pos, color):
        """
        """
        w_pix, h_pix = pos
        img[h_pix - 2: h_pix + 2, w_pix - 2: w_pix + 2, color] = 1