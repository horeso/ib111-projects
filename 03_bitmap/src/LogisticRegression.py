"""
Logistic regression class, which is used for recognizing
digits in sudoku
"""

import numpy as np

class LogisticRegression:

    # l2 = regularization parameter
    def __init__(self, classes, lr=0.0001, l2=0.01, batch_size=512):
        self.lr = lr
        self.l2 = l2
        self.batch_size = batch_size
        self.class_num = classes
    
    
    def train(self, train_X, Y, iters, val_X, val_Y):
        X = np.copy(train_X)
        X = np.hstack((np.ones((X.shape[0], 1)), X))
        Y = np.copy(Y)
        images, features = X.shape
        
        # we have model for each class / 1 vs all
        self.models = np.zeros((self.class_num, features))
        print('models shape:', self.models.shape, self.models[0].shape)
        
        index = 0
        
        for i in range(iters):
            X_batch, Y_batch, index = self.get_next_batch(X, Y, index, self.batch_size)
            
            for m in range(1, self.class_num+1):
                model = self.models[m-1]
                
                # create correct labels for given model
                labels = self.get_labels_for_model(Y_batch, m)
                
                f = X_batch.dot(model)
                h = self.sigmoid(f)
                dif = labels - h
                
                # square loss function
                loss = np.sum((dif)**2) + self.l2 * np.sum(model[1:]**2)
                loss /= 2 * self.batch_size
                
                # compute gradient
                grad_bias = - X_batch.T.dot(dif * h * (1 - h))
                grad = grad_bias[1:] + self.l2 * np.sum(model[1:])
                grad /= self.batch_size
                grad_bias /= self.batch_size
                
                # model update, using sgd
                model[1:] -= self.lr * grad
                model[0] -= self.lr * grad_bias[0]
        
            if i % 100 == 0:
                print('step: ',i,' validation accuracy:', round(self.test(val_X, val_Y), 3))

        print('Finished with val accuracy: ', round(self.test(val_X, val_Y), 3))
        
    
    def get_next_batch(self, X, Y, index, batch_size):
        samples, _ = X.shape

        if index >= samples - batch_size:
            p = np.random.permutation(samples)
            X = X[p]
            Y = Y[p]
            index = 0

        X_batch = X[index : index + batch_size]
        Y_batch = Y[index : index + batch_size]

        index += batch_size

        return X_batch, Y_batch, index

    def get_labels_for_model(self, Y_batch, m):
        labels = np.copy(Y_batch)
        labels[Y_batch != m] = 0
        labels[Y_batch == m] = 1
        return labels

    def test(self, X, Y):            
        X1 = np.hstack((np.ones((X.shape[0], 1)), X))
        
        pred_label = self.sigmoid(X1.dot(self.models.T))
        pred_label = pred_label.argmax(1) + 1
        
        correct = pred_label[pred_label == Y]
        
        return correct.shape[0] / pred_label.shape[0]
    
    def predict(self, X):
        
        pixels = X.shape[0]
        X1 = np.hstack((1, X))
        
        pred_label = self.sigmoid((self.models.dot(X1)))
#         print(pred_label)
#         pred_label = self.models.dot(X1)
        return pred_label
    
    def save_model(self, filename):
        np.save(filename, self.models)
        
    def load_model(self, filename):
        self.models = np.load(filename)
        
    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))
    
    def loss(self, X, y=None):
        pass