"""
Sudoku class solves the whole problem, it recognizes borders,
recognizes digits and returns solved sudoku
"""


import os
import numpy as np

from src.imgprocessing import *
import src.imgutils as imgutils
from src.Solver import Solver
from src.ImageCreator import ImageCreator
from src.LogisticRegression import LogisticRegression


class Sudoku:
    
    def __init__(self, solved_dir='data/solved'):
        self.creator = ImageCreator()
        self.solved_dir = solved_dir + '/'
        self.saving = True
        self.verbose = True
        self.filename = 'Undefined'
    
    def solve(self, path, verbose=True, saving=True):
        """
        Runs all needed functions to solve the image
        
        """
        self.saving = saving
        self.verbose = verbose
        
        self.filename = path.split('/')[-1].split('.')[0]
        
        os.mkdir(self.solved_dir + self.filename)
        
        cells, digit_cell_inds, empty_img, trans_img = self.solve_image(path)
        
        model = LogisticRegression(9)
        model.load_model('digit_recog_model.npy')
        sudoku = self.solve_digits(cells, digit_cell_inds, trans_img, model)
        
        solution = self.solve_sudoku(sudoku)
        
        empty = list(set(range(81))-set(digit_cell_inds))
        digits = [solution[ind // 9, ind % 9] for ind in empty]
        
        solved_img = self.creator.create_img_with_digits(trans_img, empty, digits, False)
        path = self.solved_dir + self.filename + '/'
        imgutils.save_image(solved_img, path + 'solution.png', True)
        
        return solved_img
        
    def solve_image(self, path):
        """
        Returns
        - cells: 
        - digit_cell_inds:
        - empty_img:
        - trans_img
        """

        img, orig_size = imgutils.read_image(path, 512, False)
        lower_dim = min(orig_size)
        downsized, orig_size = imgutils.read_image(path, lower_dim, False)
        img = conv_2d(img, GAUSS_KER_5x5)
        th = threshold(img)
        
        minsum_pos,ldsum_pos, rusum_pos, maxsum_pos = get_square_points(th, True)
        contour = find_contours(th, True)

        contour_img = self.creator.create_contour_img(th, contour, [minsum_pos, ldsum_pos, rusum_pos, maxsum_pos])
        
        if self.verbose:
            print('Scaled image')
            imgutils.show_im(img)
            print('Thresholded image')
            imgutils.show_im(th)
            print('Contour image')
            imgutils.show_im(contour_img)

        ratio = lower_dim / 512
        positions = (minsum_pos, maxsum_pos, rusum_pos, ldsum_pos)
        new_min, new_max, new_ru, new_ld = self.scale_by_ratio(positions, ratio)

        tran_size = 576
        lower_trans_img = apply_tranformation(img, [minsum_pos, ldsum_pos, rusum_pos, maxsum_pos],
                               [(0,0),(tran_size,0),(0,tran_size), (tran_size, tran_size)])
        lower_trans_img = threshold(lower_trans_img)
        lower_cells = get_cells(1 - lower_trans_img, 64)

        # locations of empty cells
        empty = get_empty_cells(lower_cells, 1)

        digit_cell_inds = list(set(range(81))-set(empty))

        empty_img = self.creator.create_img_with_empties(lower_trans_img, empty)
        
        trans_img = apply_tranformation(downsized, [new_min, new_ld, new_ru, new_max],
                               [(0,0),(tran_size,0),(0,tran_size), (tran_size, tran_size)])

        cells = get_cells(trans_img, 64)

        if self.verbose:
            imgutils.show_im(lower_trans_img)
            print('Image with X in empty cells')
            imgutils.show_im(empty_img)

        if self.saving:
            path = self.solved_dir + self.filename + '/'
            imgutils.save_image(img, path + 'lower_res.png')
            imgutils.save_image(th, path + 'threshold.png')
            imgutils.save_image(contour_img, path + 'contour.png', True)
            imgutils.save_image(lower_trans_img, path + 'trans_thresh.png')
            imgutils.save_image(empty_img, path + 'empty_img.png', True)

        return cells, digit_cell_inds, empty_img, trans_img
    
    def scale_by_ratio(self, positions, ratio):
        minsum, maxsum, rusum, ldsum = positions

        ratio = (ratio, ratio)
        def integerize_vec(x) : return (int(x[0]), int(x[1]))
        def mult_vec(x, y) : return (x[0] * y[0], x[1] * y[1])

        new_min = integerize_vec(mult_vec(minsum, ratio))
        new_max = integerize_vec(mult_vec(maxsum, ratio))
        new_ru = integerize_vec(mult_vec(rusum, ratio))
        new_ld = integerize_vec(mult_vec(ldsum, ratio))

        return new_min, new_max, new_ru, new_ld

    
    def solve_digits(self, cells, indices, trans_img, model):
        """
        Recognizes digits and saves them or shows them

        Inputs:
        - cells: numpy array of shape (cell_num, 64, 64)
        - indices: list of positions of cells
        - trans_img: binarized image which is used for visualisation
        - model: instance that has implemented 'predict' method
        """
        digits = []
        sudoku = np.zeros((9, 9))
    
        if self.saving:
            path = self.solved_dir + self.filename + '/'
            os.mkdir(path+'cells')
            path = path + 'cells/'
    
        for ind in indices:
            cell = cells[ind]
            pro_cell, cont_img, cont, corners = process_cell(cell)
            contour_img = self.creator.create_contour_img(cont_img, cont, corners)
            
            if self.saving:
                imgutils.save_image(contour_img, path+str(ind)+'.png', True)
            
            if self.verbose:
                imgutils.show_im(cell)
                imgutils.show_im(contour_img)
                imgutils.show_im(pro_cell)
                print('*'*20)
            
            label = model.predict(pro_cell.reshape(4096))
            y = label.argmax() + 1
            digits.append(y)

            col = ind % 9
            row = ind // 9

            sudoku[row, col] = y
        
        if self.verbose:
            img_with_digits = self.creator.create_img_with_digits(trans_img, indices, digits, False)
            imgutils.show_im(img_with_digits)
        if self.saving:
            img_with_digits = self.creator.create_img_with_digits(trans_img, indices, digits, False)
            imgutils.save_image(img_with_digits, path + 'recognized.png', True)
        
        return sudoku
        
    def solve_sudoku(self, sudoku):
        """
        Uses Solver class to solve the puzzle

        Inputs:
        - sudoku: numpy 2d array with sudoku

        Returns array with solved sudoku
        """
        solver = Solver()
        solver.solve(sudoku)
        
        return solver.solution.astype(int)
    
