from os import listdir
from os.path import isfile, join
import numpy as np
from PIL import Image

def get_file_names(data_dir):
    """
    Finds filenames of all training data
    """
    digit_dirs = [fold for fold in listdir(data_dir)]
    print(digit_dirs)
    
    # dict where key is digit number, value is list of filenames
    filenames = {}
    
    for digit_dir in digit_dirs:
        filenames[digit_dir] = [data_dir+'/'+ digit_dir+'/'+f for f in listdir(data_dir+'/'+digit_dir)]
        
    return filenames

def load_data(data_dir, shape):
    """
    Loads training data

    Returns:
    - training images
    - training labels
    """
    filenames = get_file_names(data_dir)
    
    total_images = sum([len(item_list[1]) for item_list in filenames.items()])
    print('All images nummber: ', total_images)
    
    X = np.zeros((total_images, shape[0], shape[1]))
    Y = np.zeros((total_images))
    
    index = 0
    
    for digit in filenames.keys():
        for path_image in filenames[digit]:
            img = read_img(path_image, shape)
            X[index, :, :] = img / 255
            Y[index] = int(digit)
            
            index += 1
            
    perm = np.random.permutation(total_images)
            
    return X[perm], Y[perm].astype(np.int32)
    
def read_img(path, shape):
    img = Image.open(path).resize(shape)
    
    new_img = np.zeros(shape)
    
    for i in range(shape[0]):
        for j in range(shape[1]):
            new_img[j, i] = img.getpixel((i, j))
            
    return new_img

def flatten_X(X):
    """
    Flattens images for usage in classifier
    """
    images, x, y = X.shape
    return X.reshape((images, x * y))

def split_train_test(X, Y, ratio):
    """
    Splits data into train/test sets

    ratio: ratio of samples in train set
    """
    image_num = X.shape[0]
    first_test = int(ratio * image_num)
    return X[:first_test], Y[:first_test], X[first_test:], Y[first_test:]