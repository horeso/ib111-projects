"""
This module provides useful functions used for image processing
"""


from PIL import Image
import numpy as np

import src.imgutils as imgutils


GAUSS_KER_5x5 = np.array([[1, 4, 6, 4, 1], [4, 16, 24, 16, 4], [
                         6, 24, 36, 24, 6], [4, 16, 24, 16, 4], [1, 4, 6, 4, 1]]) / 256.0


def dilate_erode(img, dilate):
    """
    Dilates or erodes image

    Inputs:
    - img : np array
    - dilate : True->dilate image
               False-> erode image

    Returns new dilated/eroded image
    """
    value = 1 if dilate else 0

    rot_matrix = np.array([[0, -1], [1, 0]])
    new_img = np.copy(img)

    for x in range(img.shape[1]-1):
        for y in range(img.shape[0]-1):
            if img[y, x] == value:
                pos = np.array([x, y])
                direction = np.array([1, 0])
                for i in range(9):
                    new_img[pos[1]+direction[1], pos[0]+direction[0]] = value
                    direction = rot_matrix.dot(direction)

    return new_img


def dilate(img):
    return dilate_erode(img, True)


def erode(img):
    return dilate_erode(img, False)


def closing(img):
    return erode(dilate(img))


def threshold(img, block_size=7, c=0.0196):
    """
    Binarizes image using adaptive thresholding.
    We first create threshold image using convolution with mean
    kernel, then we compare that threshold with image 
    """
    threshold = conv_2d(img, get_mean_ker(block_size)) - c

    high = np.where(img > threshold)
    new_img = np.ones(img.shape)
    new_img[high] = 0

    return new_img


def get_mean_ker(size):
    ker = np.ones((size, size))
    ker /= size**2
    return ker


def get_mean_ker_x(size):
    """
    Returns mean kernel 1D
    """
    ker = np.ones((size, 1))
    ker /= size
    return ker


def find_nearest(array, value):
    """
    Finds nearest value to a given value in the array

    Inputs:
    - array: numpy array
    - value: 

    Returns closest value
    """
    return abs(array - value).argmin()


def get_square_points(img, from_mid=False):
    """
    Finds corners of the contour. We first initialize positions
    to first position in the image, we then go through all contour
    positions. Left-top corner has lowest sum of 'x' and 'y' coordinate.
    Right-down corner has highest sum... etc.

    Inputs:
    - img: np array
    - from_mid: if True then only search for contour 
                from the middle of the image

    Returns positions of corners of sudoku.
    """
    contour = find_contours(img, from_mid)

    init_sum = contour[0][1] + contour[0][0]
    init_pos = contour[0]

    minsum = init_sum
    minsum_pos = init_pos

    maxsum = init_sum
    maxsum_pos = init_pos

    ldsum = init_sum
    ldsum_pos = init_pos

    rusum = init_sum
    rusum_pos = init_pos

    for con in contour:
        if con[1] + con[0] < minsum:
            minsum = con[1] + con[0]
            minsum_pos = con

        if con[1] + con[0] > maxsum:
            maxsum = con[1] + con[0]
            maxsum_pos = con

        if img.shape[0] - con[0] + con[1] < rusum and con[0] > con[1]:
            rusum = img.shape[0] - con[0] + con[1]
            rusum_pos = con

        if img.shape[1] - con[1] + con[0] < ldsum and con[1] > con[0]:
            ldsum = img.shape[1] - con[1] + con[0]
            ldsum_pos = con

    return minsum_pos, ldsum_pos, rusum_pos, maxsum_pos


def edge_detection(img):
    """
    Edge detector which uses sobel filters,
    is not used anywhere in the project now.
    """

    sobel_x = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
    sobel_y = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])

    x_grad = conv_2d(img, sobel_x)
    y_grad = conv_2d(img, sobel_y)

    grad = np.sqrt(x_grad**2 + y_grad**2)
    theta = np.arctan2(y_grad, x_grad)

    return grad, theta


def conv_2d_old(img, kernel, padding='same', step=[1, 1]):
    """
    Convolves image with given kernel

    OLD VERSION, BETTER AND FASTER BELOW
    """
    img_shape = img.shape
    ker_shape = kernel.shape

    new_img = np.zeros(img.shape)
    # print(kernel.shape)
    if ker_shape[0] != ker_shape[1]:
        print("Kernel should be square matrix")

    ker_half = ker_shape[0] // 2

    for i in range(img_shape[0]):
        for j in range(img_shape[1]):

            accu = 0

            if i < ker_half or j < ker_half or i >= img_shape[0]-ker_half \
                    or j >= img_shape[1]-ker_half:

                new_img[i, j] = 0
                continue

            for k in range(ker_shape[0]):
                row_ind = i + k - ker_half
                if row_ind < 0 or row_ind >= img_shape[0]:
                    accu = 0
                    break

                for l in range(ker_shape[1]):
                    col_ind = j + l - ker_half
                    if col_ind < 0 or col_ind >= img_shape[1]:
                        accu = 0
                        break

#                     print(row_ind, col_ind, k, l)
                    accu += img[row_ind, col_ind] * kernel[k, l]

            new_img[i, j] = accu

    return new_img


def conv_2d(img, kernel):
    """
    Convolution implemented so it fits this project, certainly
    not very general, input kernel has to be square with odd sides,
    stride is always one and the image is padded so the output image 
    has the same size
    """
    img_h, img_w = img.shape
    kernel_h, kernel_w = kernel.shape

    if kernel_h % 2 == 0 or kernel_h != kernel_w:
        print('Warning: use square kernels with odd side')

    pad = (kernel_h - 1) // 2
    padded_img = np.lib.pad(img, ((pad, pad), (pad, pad)), 'constant')

    new_img = np.zeros(img.shape)

    reshaped_ker = kernel.reshape(kernel_h*kernel_w)

    for k in range(img_h):
        for l in range(img_w):
            start_h = k
            start_w = l
            end_h = start_h + kernel_h
            end_w = start_w + kernel_w

            window = padded_img[start_h:end_h, start_w:end_w]
            reshaped_win = window.reshape(kernel_h*kernel_w)
            new_img[k, l] = reshaped_ker.dot(reshaped_win)

    return new_img

# this alg. is used
# http://www.imageprocessingplace.com/downloads_V3/root_downloads/tutorials/contour_tracing_Abeer_George_Ghuneim/square.html
# contour - (col, row) = (x, y)


def find_contour(img, height):
    seq = []
    h, w = img.shape
    # (col, row)
    start_pos = (0, height)
    start_pixel = ()

    def vec_add(x, y): return (x[0]+y[0], x[1] + y[1])

    # go from left to right and find some black pixel
    while(start_pos[0] < img.shape[0]):
        if img[start_pos[1], start_pos[0]] == 1:
            start_pixel = start_pos
            break
        start_pos = vec_add(start_pos, (1, 0))

    # did not find any starting pix
    if start_pixel == ():
        return ()

    seq.append(start_pixel)
    direction = (0, 1)
    cur_pix = vec_add(start_pixel, direction)

    # until we get to start
    while(cur_pix != start_pixel):
        if cur_pix[0] >= w or cur_pix[1] >= h:
            return ()
        # case when we step on next contour pixel
        if img[cur_pix[1], cur_pix[0]] == 1:
            seq.append(cur_pix)
            direction = (-direction[1], direction[0])
            cur_pix = vec_add(cur_pix, direction)

        # case when we step away from the contour
        else:
            direction = (direction[1], -direction[0])
            cur_pix = vec_add(cur_pix, direction)

    return seq


def find_contours(img, from_mid=False):
    """
    Finds contours of sudoku
    """
    max_area = 0
    best_contour = 0

    h, w = img.shape

    if from_mid:
        return find_contour(img, h // 2)

    step = h // 16

    for i in range(10, h-10, step):
        con = find_contour(img, i)
        are = contour_area(con)

        if are > max_area:
            max_area = are
            best_contour = con

    return best_contour


def contour_area(contour):
    """
    Computes the area of given contour.
    This is not very general and works only for non self intersecting
    contours.

    Inputs:
    - contour: list of contour positions

    Returns area of the contour
    """

    if contour == ():
        return 0

    pos1 = contour[0]
    area = 0
    for pos2 in contour[1:]:
        area += abs(pos1[0] * pos2[1] - pos1[1] * pos2[0])
        pos1 = pos2

    return area/2


def process_cell(img):
    t_cell = np.copy(img)
    t_cell = closing(1 - threshold(conv_2d(t_cell, GAUSS_KER_5x5)))

    # get rid of bordering pixels
    t_cell[:7, :] = 1
    t_cell[:, :7] = 1
    t_cell[57:, :] = 1
    t_cell[:, 57:] = 1

    cont = find_contours(1 - t_cell)
    minx, maxx, miny, maxy = get_contour_minmax(cont)

    # corners left up right down
    lu = (minx, miny)
    ld = (minx, maxy)
    rd = (maxx, maxy)
    ru = (maxx, miny)
    corners = [lu, ld, rd, ru]

    cont_img = np.copy(img)

    # for keeping aspect ratio of the digit
    ratio = (maxx - minx) / (maxy - miny)
    right_x = int(50*ratio)
    if right_x % 2 == 1:
        right_x += 1

    transformed = apply_tranformation(t_cell, [lu, ru, ld, rd],
                                      [(0, 0), (right_x, 0), (0, 50), (right_x, 50)]).T

    # imgutils.show_im(transformed)

    # insert enhanced image to 64x64 image
    pro_cell = np.ones((64, 64))
    from_ind_y = 7  # (64-50) / 2
    from_ind_x = (64 - right_x) // 2
    to_ind_y = 57
    to_ind_x = 64 - from_ind_x
    pro_cell[from_ind_y:to_ind_y, from_ind_x:to_ind_x] = transformed

    return pro_cell, cont_img, cont, corners


def get_contour_minmax(contour):
    """
    returns minx, maxx, miny, maxy coords of contour
    """
    # set initial value
    miny = contour[0][1]
    maxy = miny
    minx = contour[0][0]
    maxx = minx

    # get max, min
    for con in contour:
        if con[1] < miny:
            miny = con[1]
        elif con[1] > maxy:
            maxy = con[1]

        if con[0] < minx:
            minx = con[0]
        elif con[0] > maxx:
            maxx = con[0]

    return minx, maxx, miny, maxy


def get_centrality(img, coef):
    """ 
    measure for deciding whether the cell contains digit or not
    quadratically deacreases the value of pixel based on distance
    from the centre of the image
    """
    height, width = img.shape
    half_h = height // 2
    half_w = width // 2
    dist = 0

    for i in range(height):
        for j in range(width):
                # distance from the centre
            d = ((i - half_h)**2 + (j - half_w)**2)
            # condition for division by zero
            if d < 0.5:
                dist += 1 * coef
                continue

            dist += coef * (1 - img[i, j]) / d

    return dist


def get_empty_cells(cells, coef):
    """
    For every cell in 'cells' decides, whether it is empty or not
    """
    empty = []

    for i in range(len(cells)):
        cell = cells[i]

        centra = get_centrality(cell, 1)

        # 2.5 is manually selected threshold
        if centra <= 2.5:
            empty.append(i)

    return empty


def get_transformation(from_square, to_square):
    """
    Computes transformation matrix from from_square coordinates
    into to_square coordinates.
    """
    fpos1, fpos2, fpos3, fpos4 = from_square
    tpos1, tpos2, tpos3, tpos4 = to_square
    # from original positions
    orig_matrix = np.array([[fpos1[0], fpos2[0], fpos3[0]],
                            [fpos1[1], fpos2[1], fpos3[1]],
                            [1, 1, 1]])
    orig_vect = np.array([fpos4[0], fpos4[1], 1])

    coef = np.linalg.inv(orig_matrix).dot(orig_vect)
    A = scale_columns(orig_matrix, coef)

    # target positions
    out_matrix = np.array([[tpos1[0], tpos2[0], tpos3[0]],
                           [tpos1[1], tpos2[1], tpos3[1]],
                           [1, 1, 1]])
    out_vect = np.array([tpos4[0], tpos4[1], 1])

    coef = np.linalg.inv(out_matrix).dot(out_vect)
    B = scale_columns(out_matrix, coef)

    return A.dot(np.linalg.inv(B))


def scale_columns(orig_matrix, coef):
    """

    """
    A = np.copy(orig_matrix).astype(np.float)

    A[:, 0] *= coef[0]
    A[:, 1] *= coef[1]
    A[:, 2] *= coef[2]

    return A


def apply_tranformation(img, from_square, to_square):
    """
    Projects part of 'img' contained in 4sided polygon described
    by 'from_square' into 'to_square'.

    Returns projected image, this is used to align sudoku image,
    so it could be cutted into cells properly.
    """
    trans_mat = get_transformation(from_square, to_square)

    new_img = np.zeros((to_square[3][1], to_square[3][0]))
    height, width = new_img.shape

    # print('New Image shape: ', new_img.shape)
    # print('Old image shape:', img.shape)

    for row_out in range(height):
        for col_out in range(width):
            new_pos = trans_mat.dot(np.array([col_out, row_out, 1]))
            # j is x=column, i is y=row

            new_pos = new_pos / new_pos[2]

            row = int(round(new_pos[1]))
            col = int(round(new_pos[0]))
            if col >= img.shape[1]:
                print('correcting col in apply_trans:', row, col)
                break
#                 col = width -1
            if row >= img.shape[0]:
                print('correcting row in apply_trans:', row, col)
                break
            new_img[row_out, col_out] = img[row, col]

    return new_img.T


def get_cells(img, cell_size):
    """
    Cuts correctly aligned image into an (81, cell_size, cell_size)
    array.
    """
    cells = np.zeros((81, cell_size, cell_size))

    for i in range(9):
        for j in range(9):
            start_row = i * cell_size
            end_row = (i + 1) * cell_size
            start_col = j * cell_size
            end_col = (j + 1) * cell_size

            cell = img[start_row:end_row, start_col:end_col]
            cells[i*9 + j, :, :] = cell

    return cells
