"""
Solver solves sudoku already in the 2D array.
Recursively adds numbers, which can be added to the
puzzle. If it cannot add anything, the puzzle is solved.
"""

import numpy as np

class Solver:

    def __init__(self):
        self.solution = np.zeros((9, 9))

    def solve(self, sudoku):

        self.solution = np.zeros((9, 9))
        self.solved = False

        self.recurse(sudoku)

        return self.solution

    def recurse(self, sudoku):

        zero_ind = self.get_zero_indices(sudoku)

        if zero_ind != []:
            rand_ind = zero_ind[0]
            possible_numbers = self.get_possible_next(
                sudoku, rand_ind[0], rand_ind[1])
            for number in possible_numbers:
                new_sudoku = np.copy(sudoku)
                new_sudoku[rand_ind[0], rand_ind[1]] = number
                self.recurse(new_sudoku)

                if self.solved:
                    return

        else:
            self.solution = sudoku
            self.solved = True

    # returns array, where 0 are next possible locations to place a number
    def get_possible_next(self, sudoku, row, col):
        col_numbers = sudoku[:, col].tolist()
        row_numbers = sudoku[row, :].tolist()

        row_ind1 = row - row % 3
        row_ind2 = row + 3 - row % 3
        col_ind1 = col - col % 3
        col_ind2 = col + 3 - col % 3

        cell_numbers = sudoku[row_ind1:row_ind2,
                              col_ind1:col_ind2].flatten().tolist()

        used_set = set(row_numbers).union(
            set(col_numbers)).union(set(cell_numbers))



        return list(set(range(1, 10)).difference(used_set).difference(set([0])))

    # returns tuple of (row, col), where sudoku has zero value
    def get_zero_indices(self, sudoku):
        rows, cols = np.where(sudoku == 0)

        return list(zip(rows.tolist(), cols.tolist()))
