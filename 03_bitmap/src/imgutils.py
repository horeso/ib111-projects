from PIL import Image
import numpy as np
import copy
import matplotlib.pyplot as plt

def rgb_to_grayscale(pixel):
    return .2126 * pixel[0] + .7152 * pixel[1] + .0722 * pixel[2]

def array_rgb_to_p(img):
    return .2126 * img[:,:,0] + .7152 * img[:,:,1] + .0722 * img[:,:,2]

def normalize(img):
    mini = img.min()
    maxi = img.max()
    
    return (img - mini) / (maxi - mini)

def img_to_np(img, rgb=True):
    """
    Loads PIL.Image to numpy array

    Inputs:
    - img : PIL Image object
    - rgb : True if original image was rgb

    Returns numpy array grayscale image
    """
    width, height = img.size
    
    new_img = np.array(img)
    
    if rgb:
        new_img = array_rgb_to_p(new_img)
            
    return new_img

def read_image(path, size, orig=False):
    img = ''
    orig_size = (-1, -1)
    try:
        img = Image.open(path)
        
    except:
        print("cannot load image")

    orig_img = copy.deepcopy(img)
    orig_size = img.size

    if size!=0:
        img = img.resize((size, size))

    print('Loaded: ', path)
    print("Size:", img.size)
    
    if orig:

        if img.mode == 'RGB':
            return normalize(img_to_np(img, rgb=True)), normalize(img_to_np(orig_img, rgb=True)), orig_size
        else:
            return normalize(img_to_np(img, rgb=False)), normalize(img_to_np(orig_img, rgb=False)), orig_size
    else:
        if img.mode == 'RGB':
            return normalize(img_to_np(img, rgb=True)), orig_size
        else:
            return normalize(img_to_np(img, rgb=False)), orig_size
    
def save_image(img, filename, rgb=False):
    if rgb:
        new_img = create_rgb_image(img)
    else:
        new_img = create_gray_image(img)
    
    new_img.save(filename)
    print('Image saved: ', filename)

def show_im(img):
    plt.imshow(img, cmap='gray')
    plt.show()

''' These two functions are not in ImageCreator
class because they write into files, ImageCreator
operates only with numpy arrays'''

def create_rgb_image(img):
    height = img.shape[0]
    width = img.shape[1]

    new_img = Image.new('RGB',(width, height))

    for row in range(img.shape[0]):
        for col in range(img.shape[1]):
            new_img.putpixel((col, row), tuple((img[row, col, :]*255).astype(int).tolist()))

    return new_img

def create_gray_image(img):
    height = img.shape[0]
    width = img.shape[1]

    new_img = Image.new('P', (width, height))

    for row in range(img.shape[0]):
        for col in range(img.shape[1]):
            new_img.putpixel((col, row), (int(img[row, col]*255)))

    return new_img