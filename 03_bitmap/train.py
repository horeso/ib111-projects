"""
Running this script will train the digit recognition model
"""

import numpy as np

from src.LogisticRegression import LogisticRegression
from src.LearningUtils import *

data_dir = 'data/digits'

X, Y = load_data(data_dir, (64, 64))
X = flatten_X(X)

print('X shape:', X.shape)
print('Y shape:', Y.shape)

X_train, Y_train, X_test, Y_test = split_train_test(X, Y, 0.8)

model = LogisticRegression(9, lr=0.003, l2=0.01, batch_size=512)
model.train(X_train, Y_train, 5000, X_test, Y_test)

model.save_model('digit_recog_model_new.npy')