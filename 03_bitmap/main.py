"""
	Usage: 
		- put photos in .png/.jpg format into 'data/unsolved_sukodu' folder
		- make sure there is not already solved photo of the
		  same name in the 'data/solved' folder
		- run this script
"""
import os
import shutil
from os import listdir
from os.path import isfile, join
import numpy as np

from src.Sudoku import Sudoku

def main():
    
    mypath = 'data/unsolved_sudoku/'
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    to_solve = list(map(lambda x: mypath+x, onlyfiles))

    for f in to_solve:
        s = Sudoku('data/solved')
        solved = s.solve(f, verbose=False, saving=True)

if __name__ == '__main__':
    main()