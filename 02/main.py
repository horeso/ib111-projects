from src.data_processing import *

sal_by_type, deg_payback, sal_by_region = load_data()

plots_dir = 'plots'
# print(sal_by_type.table)

by_school_type(sal_by_type, plots_dir)
degree_growth(deg_payback, plots_dir)
average_growth(sal_by_type, plots_dir)
by_region(sal_by_region, plots_dir)
highest_by_school(sal_by_type, plots_dir)
highest_by_deg(deg_payback, plots_dir)
growth_2d_hist(sal_by_region, plots_dir)
by_region_scatter(sal_by_region, plots_dir)
by_types_scatter(sal_by_type, plots_dir)