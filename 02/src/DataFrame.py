"""
DataFrame provides interface to work with data loaded into it
"""

class DataFrame:

    def __init__(self, table={}):
        if type({}) != type(table):
            print("Table argument is not dictionary")

        self.table = table
        self.index = ''
        self.index_col = ''

        if table != {}:
            fst_key = list(table.keys())[0]
            self.index = table[fst_key]
            self.index_col = fst_key

    def __getitem__(self, key):
        return self.table[key]

    def loc(self, column_name):
        return self.table[column_name]

    # returns lines having value in given column
    def get_lines(self, column, value):
        row_indices = []
        for i in range(len(self.table[column])):
            item = self.table[column][i]

            if item == value:
                row_indices.append(i)
        return [self.get_row_int_index(i) for i in row_indices]

    def get_row_int_index(self, index):
        row = {}

        for i in range(len(self.index)):
            if i == index:
                for key in self.table.keys():
                    row[key] = [self.table[key][i]]
                break

        return row

    # returns row on given index
    def get_row(self, index):
        row = {}

        for i in range(len(self.index)):
            if self.index[i] == index:
                for key in self.table.keys():
                    row[key] = [self.table[key][i]]
                break

        return row

    def set_index(self, key):
        if key in self.table.keys():
            self.index = self.table[key]
            self.index_col = key
        else:
            print('key not found')

    def get_rows(self, indices):
        new_table = self.get_row(indices[0])

        cols = list(new_table.keys())

        for ind in indices[1:]:
            row = self.get_row(ind)

            for key in cols:
                new_table[key].extend(row[key])

        out_df = DataFrame(new_table)
        out_df.set_index(self.index_col)
        return out_df

    def get_column(self, key):
        """
        Returns column as tuple ([index], [value])
        """
        if key in self.table.keys():
            return list(zip(self.index, self.table[key]))

    def value_counts(self, column):
        """
        Returns counts of distinct values of 'column'
        """
        series = self.table[column]
        distinct = list(set(series))

        counts = {}

        for item in distinct:
            count = series.count(item)

            counts[item] = count

        return counts

    def agg(self, f, on_col, val_col):
        """
        Aggregates data on 'on_col' using function 'f'
        and values from 'val_col'

        Inputs:
        - f: Function [a] -> b
        - on_col: Column we aggregate on
        - val_col: Column we use values from 

        Returns list of tuples (key, value)
        """
        on_series = self.table[on_col]
        val_series = self.table[val_col]
        distinct = list(set(on_series))

        out_list = []

        for on_name in distinct:
            indices = self.get_indices(on_series, on_name)
            list_to_agg = self.get_list_on_ind(val_series, indices)
            agg_val = f(list_to_agg)

            out_list.append((on_name, agg_val))

        return out_list

    def map_two(self, f, col1, col2):
        """
        Maps `f` on two columns
        """
        values = list(map(f, self.table[col1], self.table[col2]))
        return list(zip(self.index, values))

    def get_list_on_ind(self, in_list, indices):
        out_list = []

        s_indices = sorted(indices)

        for i in s_indices:
            out_list.append(in_list[i])

        return out_list

    def get_indices(self, in_list, value):
        """
        Returns indices of items where 'in_list' has given 'value'
        """
        out_list = []
        for i in range(len(in_list)):
            if value == in_list[i]:
                out_list.append(i)

        return out_list

    def add_column(self, data, col_name):
        """
        Add column to the dataframe, data must have same length
        as the index has
        """
        index_len = len(self.index)
        if index_len != len(data):
            print('Data has different lenght from index lenght')
            print(index_len, len(data))
            return

        self.table[col_name] = data

    def get_n_best(self, f, count, column):
        """
        Returns 'count' values, which are biggest
        according to some ordering function 'f'

        Inputs:
        - f: compare function f(a, b) -> Bool
        - count: how many results
        - column: on which column

        Returns list of 'biggest' values
        """
        data = self.table[column][:]
        remaining = list(zip(self.index, data))
        best = []

        for i in range(count):
            maxi = remaining[0]

            for item in remaining:
                if f(item[1], maxi[1]):
                    maxi = item
            best.append(maxi)
            remaining.remove(maxi)
        return best
