import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

def fst(x): return x[0]
def snd(x): return x[1]
def unzip(x): return (list(map(fst, x)), list(map(snd, x)))
def avg(x): return int(sum(x) / len(x))
def rel_change(x, y): return round((y - x) / x * 100)

class Graph_creator:
    
    def __init__(self):
        pass
    
    def bar_graph(self, tuples, filename, xlabel, ylabel, color, width=0.35, rotation=45, maxy=-1, title=""):
        attrs, vals = unzip(tuples)
        index = [i for i in range(len(attrs))]
            
        plt.bar(index, vals, width=[0.35], color=color)
        
        if maxy != -1:
            plt.ylim(0, maxy)
        
        if title != "":
            plt.title(title)
        
        if xlabel != '':
            plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.xticks(index, attrs, rotation=rotation)
        plt.savefig(filename, bbox_inches='tight')
        print('Saved:', filename)
        plt.close()
    
    def twobar_graph(self, tuples1, tuples2, filename, xlabel, ylabel, legend, color, width=0.35, rotation=45, title=""):
        attrs, vals1 = unzip(tuples1)
        _, vals2 = unzip(tuples2)
        
        fig = plt.figure()
        ax = fig.add_subplot(111)
        
        ind1 = list(range(len(vals1)))
        ind2 = list(map(lambda x: x + width, ind1))
        
        left = ax.bar(ind1, vals1, width=width, color=color[1])
        right = ax.bar(ind2, vals2, width=width, color=color[0])
        
        if title != "":
            plt.title(title)
        
        xTickMarks = attrs
        ax.set_xticks(ind2)
        xtickNames = ax.set_xticklabels(xTickMarks)
        plt.setp(xtickNames, rotation=rotation, fontsize=10)
        
        ax.legend((left[0], right[0]), legend)
        
        if xlabel != '':
            plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.xticks(rotation=rotation)
        plt.savefig(filename, bbox_inches='tight')
        print('Saved:', filename)
        plt.close()
        
    def hist_graph(self, values, filename, xlabel, ylabel, color, bins=10, title=""):
        if title != "":
            plt.title(title)
        
        plt.hist(values, bins=bins)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.savefig(filename, bbox_inches='tight')
        print('Saved:', filename)
        plt.close()

    def two_d_hist(self, start, change):
        plt.hist2d(start, change, bins = 20)
        plt.colorbar()
        plt.xlabel('Starting salary')
        plt.ylabel('% Growth')
        plt.title('Starting salary vs. % growth')
        plt.savefig('plots/salary_vs_growth.png')

    def scatter_graph(self, x_values, y_values, types, colors, x_label, y_label, title, filename):
        fig = plt.figure()
        ax = fig.add_subplot(111)

        the_cols = []

        for i in range(len(types)):
            the_cols.append(ax.scatter(x_values[i], y_values[i], color=colors[i], s=10))
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        ax.legend(the_cols, types)
        plt.title(title)
        plt.savefig(filename, bbox_inches='tight')
        print('Saved:', filename)
        plt.close()