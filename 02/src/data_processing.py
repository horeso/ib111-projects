
import src.csv_parsing as csv
from src.DataFrame import DataFrame
from src.GraphCreator import Graph_creator

# USEFUL FUNCTIONS
def fst(x): return x[0]
def snd(x): return x[1]
def unzip(x): return (list(map(fst, x)), list(map(snd, x)))
def avg(x): return int(sum(x) / len(x))
def rel_change(x, y): return round((y - x) / x * 100)

def load_data():
	table = csv.read_csv('data/salaries-by-college-type.csv')
	sal_by_type = DataFrame(table)
	sal_by_type.set_index('School Name')

	table = csv.read_csv('data/degrees-that-pay-back.csv')
	deg_payback = DataFrame(table)
	deg_payback.set_index('Undergraduate Major')

	table = csv.read_csv('data/salaries-by-region.csv')
	sal_by_region = DataFrame(table)
	sal_by_region.set_index('School Name')

	return sal_by_type, deg_payback, sal_by_region

def by_school_type(sal_by_type, plots_dir):
	start = sal_by_type.agg(avg, 'School Type', 'Starting Median Salary')
	mid = sal_by_type.agg(avg, 'School Type', 'Mid-Career Median Salary')

	grapher = Graph_creator()
	grapher.twobar_graph(start, mid, plots_dir + '/sal_by_type.png', 'School type', 'Salary ($)',
                     ('Starting salary', 'Mid career salary'), ['gold', 'lightcoral'], title='Salary by school type')

def degree_growth(deg_payback, plots_dir):
	percent_change = deg_payback.map_two(rel_change, 'Starting Median Salary', 'Mid-Career Median Salary')

	grapher = Graph_creator()
	grapher.hist_graph(unzip(percent_change)[1], plots_dir+'/perc_growth_by_type.png', 'Percent growth', 'Count', color='blue')

def by_region(sal_by_region, plots_dir):
	start_by_region = sal_by_region.agg(avg,'Region','Starting Median Salary')
	mid_by_region = sal_by_region.agg(avg,'Region','Mid-Career Median Salary')

	perc = sal_by_region.map_two(rel_change, 'Starting Median Salary', 'Mid-Career Median Salary')
	sal_by_region.add_column(unzip(perc)[1], 'percentage_change')
	perc_by_region = sal_by_region.agg(avg, 'Region', 'percentage_change')
	
	grapher = Graph_creator()
	grapher.twobar_graph(start_by_region, mid_by_region, plots_dir + '/sal_by_region.png', 'Region', 'Salary ($)',
                     ('Starting salary', 'Mid career salary'), ['gold', 'lightcoral'], title="Salary by region")
	grapher.bar_graph(perc_by_region, plots_dir + '/perc_by_region.png', '',
					 'Growth in %', 'lightskyblue', title="% growth by region")

def highest_by_deg(deg_payback, plots_dir):
	percent_change = deg_payback.map_two(rel_change, 'Starting Median Salary', 'Mid-Career Median Salary')
	deg_payback.add_column(col_name='percent_change', data=unzip(percent_change)[1])
	
	highest_by_deg = deg_payback.get_n_best(lambda x, y: x > y, 7, 'percent_change')
	lowest_by_deg = deg_payback.get_n_best(lambda x, y: x < y, 7, 'percent_change')

	grapher = Graph_creator()
	grapher.bar_graph(highest_by_deg, plots_dir + '/highest_by_deg.png', '',
					 'Growth in %', 'gold', maxy=120, title='Highest % growth by major')

	grapher.bar_graph(lowest_by_deg, plots_dir + '/lowest_by_deg.png', '',
				'Growth in %', 'lightcoral', maxy=120, title='Lowest % growth by major')

def average_growth(sal_by_type, plots_dir):
	"""
	Here we take schools with highest starting salary and lowest starting salary
	and compare their salary growth
	"""
	lowest_start = sal_by_type.get_n_best(lambda x, y: x < y, 10, 'Starting Median Salary')
	lowest_df = sal_by_type.get_rows(unzip(lowest_start)[0])
	lowest_mid = lowest_df.get_n_best(lambda x, y: x < y, 10, 'Mid-Career Median Salary')
	lowest_percent = lowest_df.map_two(rel_change, 'Starting Median Salary', 'Mid-Career Median Salary')
	lowest_df.add_column(unzip(lowest_percent)[1], 'percent_growth')

	high_start = sal_by_type.get_n_best(lambda x, y: x > y, 10, 'Starting Median Salary')
	high_df = sal_by_type.get_rows(unzip(high_start)[0])
	high_mid = high_df.get_n_best(lambda x, y: x > y, 10, 'Mid-Career Median Salary')
	high_percent = high_df.map_two(rel_change, 'Starting Median Salary', 'Mid-Career Median Salary')
	high_df.add_column(unzip(high_percent)[1], 'percent_growth')

	low_avg = avg(lowest_df['percent_growth'])
	high_avg = avg(high_df['percent_growth'])

	grapher = Graph_creator()
	grapher.bar_graph([('Lowest', low_avg), ('Highest', high_avg)], plots_dir + '/perc_growth_extreme.png', '',
                  'Growth in %', 'lightskyblue', title='Highest, lowest salary (% growth)')


def growth_2d_hist(sal_by_region, plots_dir):
	per_change = sal_by_region.map_two(rel_change, 'Starting Median Salary', 'Mid-Career Median Salary')
	change = unzip(per_change)[1]
	start = unzip(sal_by_region.get_column('Starting Median Salary'))[1]

	grapher = Graph_creator()
	grapher.two_d_hist(start, change)

def by_types_scatter(sal_by_type, plots_dir):
	types = ['Engineering', 'Party', 'Liberal Arts', 'Ivy League', 'State']
	colors = ['gold', 'lightcoral', 'skyblue', 'green', 'blue']

	x_data = []
	y_data = []

	for i in range(len(types)):
	    typ = types[i]
	    schools = sal_by_type.get_lines('School Type', typ)

	    xs = []
	    ys = []

	    for school in schools:
	        start = school['Starting Median Salary']
	        mid = school['Mid-Career Median Salary']

	        xs.append(start)
	        ys.append(mid)
	        
	    x_data.append(xs)
	    y_data.append(ys)

	grapher = Graph_creator()
	grapher.scatter_graph(x_data, y_data, types, colors, 'Starting salary',
						 'Mid career salary', 'By University type',
						 'plots/scatter_type.png')


def by_region_scatter(sal_by_region, plots_dir):
	regions = ['California', 'Northeastern', 'Western', 'Midwestern', 'Southern']
	colors = ['gold', 'lightcoral', 'skyblue', 'green', 'blue']

	x_data = []
	y_data = []

	for i in range(len(regions)):
	    region = regions[i]
	    schools = sal_by_region.get_lines('Region', region)

	    xs = []
	    ys = []

	    for school in schools:
	        start = school['Starting Median Salary']
	        mid = school['Mid-Career Median Salary']

	        xs.append(start)
	        ys.append(mid)
	    
	    x_data.append(xs)
	    y_data.append(ys)

	grapher = Graph_creator()
	grapher.scatter_graph(x_data, y_data, regions, colors, 'Starting salary',
						 'Mid career salary', 'By Region',
						 'plots/scatter_region.png')

def highest_by_school(sal_by_type, plots_dir):
	perc_change = sal_by_type.map_two(rel_change, 'Starting Median Salary', 'Mid-Career Median Salary')
	sal_by_type.add_column(unzip(perc_change)[1],'percent_change')
	best_change = sal_by_type.get_n_best(lambda x, y: x > y, 10, 'percent_change')

	grapher = Graph_creator()
	grapher.bar_graph(best_change, plots_dir + '/highest_by_school.png', '', 'Growth in %', 'gold', rotation=90)
