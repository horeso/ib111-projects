def parse_line(line, separator=','):
    # naive split
    line = line.strip()
    splitted = line.split(separator)
    
    correct_split = []
    
    temp_word = ""
    one_word = False
    
    # joins together belonging items
    for item in splitted:
        if item[0] == '"':
            one_word = True
            temp_word = item[1:]
            
        elif item[-1] == '"':
            one_word = False
            to_append = read_salary(temp_word + item[:-1])
            correct_split.append(to_append)
            temp_word = ''
        
        elif one_word:
            temp_word = temp_word + item
        
        else:
            correct_split.append(read_salary(item))
            
    return correct_split

def read_salary(text):
    if text[0] == '$':
        first_dot = text.find('.')
        parsed = "".join(list(filter(lambda x: x in '0123546789', text[:first_dot])))
        return int(parsed)
    
    else:
        return text

def read_csv(path, separator=','):
    f = open(path, 'r')
    
    header = f.readline().strip()
    columns = parse_line(header)
    data = []
    
    while(True):
        line = f.readline().strip()
        
        if line == '':
            break
        parsed = parse_line(line)
        
        if len(parsed) != len(columns):
            print('Different column number:', len(parsed), 'expecting: ', len(columns))
            print(parsed)
        
        data.append(parsed)
        
    f.close()
            
    return create_table(columns, data)

def create_table(columns, data):
    table = {key: [] for key in columns}
    
    for item_line in data:
        index = 0
        for key in columns:
            table[key].append(item_line[index])
            index += 1
            
    return table