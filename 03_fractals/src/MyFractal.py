import math
import os

from src.SvgCreator import SvgCreator
from src.Fractal import Fractal
from src.Vec2 import Vec2
from src.MyTurtle import MyTurtle


class MyFractal(Fractal):

    def __init__(self, name):
        Fractal.__init__(self, name)

        self.turtle = MyTurtle()

    def recurse(self, depth, side, angle, rot, ratio):
        if depth <= 0:
            return

        if rot:
            this_ang = angle
        else:
            this_ang = - angle

        self.turtle.rotate(this_ang)
        self.positions.append(self.turtle.pos)
        self.turtle.forward(side)
        self.turtle.save_pos()
        self.recurse(depth - 1, side * ratio, this_ang, True, ratio)
        self.turtle.rotate(180)
        self.turtle.forward(side)
        self.positions.append(self.turtle.pos)
        self.turtle.rotate(180)
        self.turtle.rotate(this_ang)
        self.recurse(depth - 1, side * ratio, this_ang, False, ratio)
        self.turtle.rotate(180)
        self.turtle.forward(side)
        self.positions.append(self.turtle.pos)
        self.turtle.rotate(this_ang)

    def generate_fractal(self, depth, angle):
        self.turtle.pos = Vec2(0, 0)
        self.turtle.set_dir(Vec2(1, 0))

        self.positions = []

        self.recurse(depth, 1, angle, True, 0.8)

        return self.positions

    def generate_fractals(self, depth, angle, width):
        folder_path = self.name+'/'+str(angle)
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        for i in range(depth+1):
            pos = self.generate_fractal(i, angle)

            filename = folder_path + '/' + self.name + '_' + str(angle) \
                + '_' + str(i) + '.svg'

            creator = SvgCreator(filename, 640)
            creator.create_from_lines(pos, width)
