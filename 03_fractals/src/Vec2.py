"""
Vector in 2D with some operations needed for 
creating fractals
"""

import math

class Vec2:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def __str__(self):
        return 'x:'+str(self.x)+' y:'+str(self.y)
    
    def __add__(self, other):
        return Vec2(self.x + other.x, self.y + other.y)
    
    def __mul__(self, other):
        if type(other) == int or type(other) == float:
            return Vec2(self.x * other, self.y * other)

        elif isinstance(other, Vec2):
            return Vec2(self.x * other.x + self.y * other.y)
        
    
    def __rmul__(self, other):
        return self * other
    
    def __sub__(self, other):
        return self+(-other)
    
    def __neg__(self):
        return Vec2(-self.x, -self.y)
    
    
    
    def rotate(self, theta, measured='degrees'):
        """
        Rotates vector 

        Inputs:
        - theta : angle of rotation in positive direction
        - measured : if 'degrees', then angle is converted to rads

        Returns rotated Vec2
        """
        if measured == 'degrees':
            theta = math.pi*theta/180

        # multiplication by rotation matrix
        new_x = self.x*math.cos(theta)-self.y*math.sin(theta)
        new_y = self.x*math.sin(theta)+self.y*math.cos(theta)
        return Vec2(new_x, new_y)
    
    def norm(self):
        leng = self.length()
        return Vec2(self.x / leng, self.y / leng) 
    
    def length(self):
        return (self.x**2 + self.y**2)**0.5