"""
Creates svg files with fractals
"""

from src.Vec2 import Vec2

class SvgCreator:
    def __init__(self, path, size):
        self.path = path
        self.size = size
        self.resize(size)
        
        self.tail = '</svg> \n</body>\n</html>\n '
        
    def create_from_lines(self, positions, width):
        """
        Creates .svg, where consecutive positions are
        connected by lines

        Inputs:
        - positions : list of Vec2 objects
        - width : width of line 
        """
        f = open(self.path, 'w')
        f.write(self.head)
        
        if len(positions) > 0:
            positions = self.transform_pos(positions)
        
        for i in range(len(positions)-1):
            cur_pos = positions[i]
            next_pos = positions[i+1]
            f.write('<line x1="{!s}" y1="{!s}" x2="{!s}" y2="{!s}" style="stroke:rgb(0,0,0);stroke-width:{!s}" />\n'.
                   format(cur_pos.x, cur_pos.y, next_pos.x, next_pos.y, width))
        f.write(self.tail)
        f.close
        
    def create_from_dots(self, positions, dot_size, centering=True):
        """
        Creates .svg with dots on 'positions'

        Inputs:
        - positions : list of Vec2 objects
        - dot_size : dot radius
        """
        f = open(self.path,'w')
        f.write(self.head)

        if len(positions) > 0:
            positions = self.transform_pos(positions)
        
        centering_vec = Vec2(self.size /2, self.size / 2)
        
        for position in positions:
            if centering:
                centered_pos = centering_vec + position
            else:
                centered_pos = position
            
            f.write(' <circle cx="{!s}" cy="{!s}" r="{!s}" stroke="black" stroke-width="{!s}" fill="black" />\n'.
                    format(centered_pos.x,centered_pos.y, dot_size, dot_size / 2))
        f.write(self.tail)
        f.close()
    
    # positions is list of n-tuples of Vec2
    def create_from_polygons(self, positions, n, width):
        """
        Creates .svg where each 'n' consecutive
        'positions' form a polygon

        Inputs:
        - positions: list of Vec2 objects
        - n: number of sides of polygon
        - width: width of lines
        """
        f = self.prepare()
        
        if len(positions) > 0:
            # positions = self.transform_pos(positions)
            positions = [Vec2(100,200)+pos for pos in positions]

        for i in range(len(positions) // n):
            to_write = '<polygon points="'
            for j in range(n):
                to_write+= str(positions[n*i + j].x) + ',' + str(positions[n*i + j].y) + ' '
            to_write += '" style="stroke:black;stroke-width:' + str(width) + '" />\n'
            f.write(to_write)
        
        self.end(f)

    def create_dots_per_unit(self):
        f = self.prepare()

        for i in range(0, self.size, 4):
            for j in range(0, self.size, 4):
                f.write('<circle cx="{!s}" cy="{!s}" r="{!s}"/>\n'.format(i,j,(1)))


        self.end(f)
    
        
    def resize(self, size):
        self.size = size
        self.head = ' <html>\n<body>\n\t<h1>My first SVG</h1>\n\t<svg height="'+str(size)+'" width="'+str(size)+'">'


    def prepare(self):
        f = open(self.path, 'w')
        f.write(self.head)
        return f


    def end(self, f):
        f.write(self.tail)
        f.close()


    def transform_pos(self, positions):
        """
        Transforms positions so they fit tighly
        to the borders of the generated .svg image

        Inputs:
        - positions : list of Vec2 objects

        Returns transformed positions (list of Vec2)
        """
 
        if len(positions) < 2:
            return positions

        min_x = positions[0].x
        min_y = positions[0].y
        max_x = positions[0].x
        max_y = positions[0].y
        for pos in positions:
            if pos.x < min_x:
                min_x = pos.x

            elif pos.x > max_x:
                max_x = pos.x

            if pos.y < min_y:
                min_y = pos.y

            elif pos.y > max_y:
                max_y = pos.y


        min_vec = Vec2(min_x, min_y)
        max_vec = Vec2(max_x, max_y)
        new_positions = []

        for pos in positions:
            new_x = (pos.x - min_vec.x) / (max_vec.x - min_vec.x) * self.size 

            if max_vec.y == min_vec.y:
                new_y = 0.5 * self.size
            else:
                new_y = (pos.y - min_vec.y) / (max_vec.y - min_vec.y) * self.size
            new_positions.append(Vec2(new_x, new_y)) 

        return new_positions




