import math
import os

from src.SvgCreator import SvgCreator
from src.Fractal import Fractal
from src.Vec2 import Vec2
from src.MyTurtle import MyTurtle


class Sierpinski(Fractal):

    def __init__(self, name):

        Fractal.__init__(self, name)

        self.turtle = MyTurtle()

    def recurse(self, depth, side):
        if depth <= 0:
            return

        for i in range(3):
            self.turtle.forward(side)
            self.turtle.rotate(120)

            self.positions.append(self.turtle.pos)

            self.recurse(depth - 1, side / 2)

    def generate_fractal(self, depth):
        """
        Inputs:
        - depth : recursion depth

        Returns list of positions
        """
        self.turtle.pos = Vec2(0, 0)
        self.turtle.set_dir(Vec2(1, 0))

        self.positions = []

        self.recurse(depth, 1)

        return self.positions

    def generate_fractals(self, depth):
        """
        Generates images of the fractals with depth in range [1 - depth]
        """
        folder_path = self.name
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        for i in range(depth+1):
            pos = self.generate_fractal(i)

            filename = folder_path + '/' + self.name + '_' + str(i) + '.svg'

            creator = SvgCreator(filename, 640)
            creator.create_from_lines(pos, 1.0)
