import math
import os

from src.SvgCreator import SvgCreator
from src.Fractal import Fractal
from src.Vec2 import Vec2
from src.MyTurtle import MyTurtle


class LevC(Fractal):

    def __init__(self, name):
        Fractal.__init__(self, name)

        self.turtle = MyTurtle()

    def recurse(self, depth):
        if depth <= 0:
            self.turtle.forward(1)
            self.positions.append(self.turtle.pos)
            return
        
        self.turtle.rotate(-45)

        self.recurse(depth - 1)

        self.turtle.rotate(90)

        self.recurse(depth - 1)

        self.turtle.rotate(-45)

    def generate_fractal(self, depth):
        self.turtle.pos = Vec2(0, 0)
        self.turtle.set_dir(Vec2(1, 0))

        self.positions = []

        self.recurse(depth)

        return self.positions

    def generate_fractals(self, depth):
        folder_path = self.name+'/'
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        for i in range(depth+1):
            pos = self.generate_fractal(i)

            filename = folder_path + '/' + self.name \
                        + '_' + str(i) + '.svg'

            creator = SvgCreator(filename, 640)
            creator.create_from_lines(pos, 1)

