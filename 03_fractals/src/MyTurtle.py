"""
Simulates functionality of Turtle module
without rendering, just saves positions
"""

from src.Vec2 import Vec2

class MyTurtle:
    def __init__(self):
        self.positions = []
        self.pos = Vec2(0,0)
        self.direction = Vec2(1,0)
        
    def reset(self):
        self.positions = []
    
    def save_pos(self):
        self.positions.append(self.pos)
    
    def forward(self, amount):
        self.pos = self.pos + self.direction * amount
        
    def rotate(self, angle):
        """
        Inputs:
        - angle : angle in degrees
        """
        self.direction = self.direction.rotate(angle)
        
    def set_dir(self, vec):
        self.direction = vec.norm()