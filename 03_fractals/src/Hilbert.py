import math
import os

from src.SvgCreator import SvgCreator
from src.Fractal import Fractal
from src.Vec2 import Vec2
from src.MyTurtle import MyTurtle


class Hilbert(Fractal):

    def __init__(self, name):
        Fractal.__init__(self, name)

        self.turtle = MyTurtle()

    def recurse(self, depth, mode):
        if depth == 0:
            return

        if mode:
            self.turtle.rotate(90)
        
            self.recurse(depth - 1, False)

            self.turtle.forward(1)
            self.positions.append(self.turtle.pos)

            self.turtle.rotate(-90)

            self.recurse(depth - 1, True)

            self.turtle.forward(1)
            self.positions.append(self.turtle.pos)

            self.recurse(depth - 1, True)

            self.turtle.rotate(-90)

            self.turtle.forward(1)
            self.positions.append(self.turtle.pos)

            self.recurse(depth - 1, False)

            self.turtle.rotate(90)
            
        else:
            self.turtle.rotate(-90)

            self.recurse(depth - 1, True)

            self.turtle.forward(1)
            self.positions.append(self.turtle.pos)

            self.turtle.rotate(90)

            self.recurse(depth - 1, False)

            self.turtle.forward(1)
            self.positions.append(self.turtle.pos)

            self.recurse(depth - 1, False)

            self.turtle.rotate(90)

            self.turtle.forward(1)
            self.positions.append(self.turtle.pos)

            self.recurse(depth - 1, True)

            self.turtle.rotate(-90)

    def generate_fractal(self, depth):
        self.turtle.pos = Vec2(0, 0)
        self.turtle.set_dir(Vec2(1, 0))

        self.positions = []

        self.recurse(depth, True)

        return self.positions

    def generate_fractals(self, depth):
        folder_path = self.name+'/'
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        for i in range(depth+1):
            pos = self.generate_fractal(i)

            filename = folder_path + '/' + self.name \
                        + '_' + str(i) + '.svg'

            creator = SvgCreator(filename, 640)
            creator.create_from_lines(pos, 1)
