import math
import os

from src.SvgCreator import SvgCreator
from src.Fractal import Fractal
from src.Vec2 import Vec2
from src.MyTurtle import MyTurtle


class PythagorasTree(Fractal):

    def __init__(self, name):
        Fractal.__init__(self, name)

        self.turtle = MyTurtle()

    def recurse(self, depth, angle, ratio, side, rot_dir):
        if depth <= 0:
            return

        # requires different rotation direction / clockwise, anticlockwise
        if rot_dir:
            square_angle = 90
        else:
            square_angle = -90

        # creates square
        zero_pos = self.turtle.pos
        self.turtle.forward(side)
        first_pos = self.turtle.pos
        self.turtle.rotate(square_angle)
        self.turtle.forward(side)
        second_pos = self.turtle.pos
        self.turtle.rotate(square_angle)
        self.turtle.forward(side)
        third_pos = self.turtle.pos

        # saves square coords
        self.positions.append(zero_pos)
        self.positions.append(first_pos)
        self.positions.append(second_pos)
        self.positions.append(third_pos)

        # clockwise / anticlockwise
        if rot_dir:
            first_vert = first_pos
            second_vert = second_pos
        else:
            first_vert = second_pos
            second_vert = first_pos

        # prepares for drawing next right square
        self.turtle.pos = first_vert
        self.turtle.set_dir(first_vert - second_vert)
        self.turtle.rotate(angle)

        self.recurse(depth - 1, angle, ratio, side * ratio, True)

        # prepares for drawing next left square
        self.turtle.pos = second_vert
        self.turtle.set_dir(second_vert - first_vert)
        self.turtle.rotate(-angle)

        self.recurse(depth - 1, angle, ratio, side * ratio, False)

    def generate_fractal(self, depth, angle, side):
        self.turtle.pos = Vec2(0, 0)
        self.turtle.set_dir(Vec2(1, 0))

        half_angle_rads = angle / 360 * math.pi
        # ratio calculated from the triangle constructed over the square
        ratio = 1 / (2 * math.sin(half_angle_rads))

        self.positions = []

        self.recurse(depth, angle / 2, ratio, side, True)

        return self.positions

    def generate_fractals(self, depth, angle, width, side):
        folder_path = self.name+'/'+str(angle)
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        for i in range(depth+1):
            pos = self.generate_fractal(i, angle, side)

            filename = folder_path + '/' + self.name + '_' + str(angle) \
                + '_' + str(i) + '.svg'

            creator = SvgCreator(filename, 640)
            creator.create_from_polygons(pos, 4, width)
