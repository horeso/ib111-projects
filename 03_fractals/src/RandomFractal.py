"""
This is not using recursion.
youtube channel Numberphile presents this
idea in https://www.youtube.com/watch?v=kbKtFN71Lfs

We basically have points in plane and some number R = (0.0 - 1.0),
at every iteration we select random point and we make a step in its
direction, lenght of this step defined as Distance_to_point * R
"""

import math
import os
import random

from src.SvgCreator import SvgCreator
from src.Fractal import Fractal
from src.Vec2 import Vec2
from src.MyTurtle import MyTurtle


class RandomFractal(Fractal):

    def __init__(self, name):
        Fractal.__init__(self, name)

        self.turtle = MyTurtle()

    def recurse(self):
        pass

    def generate_fractal(self, vert_number, step_ratio, iters):
        self.turtle.pos = Vec2(0, 0)
        self.turtle.set_dir(Vec2(1, 0))

        self.positions = []
        # prepare points
        vertices = []
        number = vert_number
        side = 400
        self.turtle.pos = Vec2(-200, -200)
        for i in range(number):
            self.turtle.forward(side)
            self.turtle.rotate(360 / number)
            vertices.append(self.turtle.pos)

        self.turtle = MyTurtle()

        for i in range(iters):

            index = random.randint(0, number - 1)

            self.turtle.pos = (self.turtle.pos + vertices[index]) * step_ratio
            self.positions.append(self.turtle.pos)

        return self.positions

    def generate_fractals(self, vert_number, step_ratio, iters, dot_size):
        folder_path = self.name
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        pos = self.generate_fractal(vert_number, step_ratio, iters)

        filename = folder_path + '/' + self.name \
            + '_' + str(vert_number) + '.svg'

        creator = SvgCreator(filename, 640)
        creator.create_from_dots(pos, dot_size)
