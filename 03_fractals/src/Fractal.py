"""
Base abstract fractal class
"""

class Fractal:
	def __init__(self, name):
		self.name = name
		self.positions = []

	def recurse(self):
		print('This should be implemented in children class')


	def generate_fractal(self, depth):
		print('This should be implemented in children class')