import math
import os
import random

from src.SvgCreator import SvgCreator
from src.Fractal import Fractal
from src.Vec2 import Vec2
from src.MyTurtle import MyTurtle

"""
This fractal should resemble a branch of a tree,
with increasing depth, the probability of creating new
branch is decreasing, also the angle of new branch is altered
randomly
"""


class BranchFractal(Fractal):

    def __init__(self, name):
        Fractal.__init__(self, name)

        self.turtle = MyTurtle()

    def recurse(self, depth, side, angle, branches, prob):
        if depth <= 0:
            return

        for i in range(branches):
            self.turtle.forward(side / (branches+1))
            self.positions.append(self.turtle.pos)

            # l_or_r = random.random() - 0.5
            # l_or_r = l_or_r / abs(l_or_r)
            l_or_r = 1

            if random.random() < prob:
                
                self.turtle.rotate(l_or_r * angle)

                new_angle = (random.random() - 0.5) * 5 + angle

                self.recurse(depth - 1, side * 0.5, new_angle, branches, prob - 0.05)

                self.turtle.rotate(-l_or_r * angle)


        self.turtle.forward(side / (branches+1))
        self.turtle.rotate(180)
        self.positions.append(self.turtle.pos)
        self.turtle.forward(side)
        self.turtle.rotate(180)

        self.positions.append(self.turtle.pos)

    def generate_fractal(self, depth, angle, branches, prob):
        self.turtle.pos = Vec2(50, 50)
        self.turtle.set_dir(Vec2(1, 0))

        self.positions = []

        self.recurse(depth, 1, angle, branches, prob)

        return self.positions

    def generate_fractals(self, depth, angle, branches, prob, width):
        folder_path = self.name+'/'+str(angle)
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        for i in range(depth+1):
            pos = self.generate_fractal(i, angle, branches, prob)


            # for p in pos:
                # print(p, end=" ")

            filename = folder_path + '/' + self.name + '_' + str(angle) \
                + '_' + str(i) + '.svg'

            creator = SvgCreator(filename, 640)
            # creator.create_from_dots(pos, width, False)
            creator.create_from_lines(pos, width)
