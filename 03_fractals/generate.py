from src.SvgCreator import SvgCreator
from src.PythagorasTree import PythagorasTree
from src.Sierpinski import Sierpinski
from src.MyFractal import MyFractal
from src.BranchFractal import BranchFractal
from src.Hilbert import Hilbert
from src.DragonCurve import DragonCurve
from src.LevC import LevC

tree = BranchFractal('Branch_fractal')
tree.generate_fractals(20, 20, 2, 1.0, 5)

tree = PythagorasTree('Pythagoras_tree')
tree.generate_fractals(12, 60, 0.4, 40)
print('Generated: ', tree.name)

tree = PythagorasTree('Pythagoras_tree')
positions = tree.generate_fractals(12, 90, 0.4, 80)
print('Generated: ', tree.name)


tree = PythagorasTree('Pythagoras_tree')
tree.generate_fractals(12, 85, 0.4, 80)
print('Generated: ', tree.name)

tree = Sierpinski('Sierpinski_triangle')
tree.generate_fractals(10)
print('Generated: ', tree.name)

tree = MyFractal('My_fractal')
tree.generate_fractals(14, 45, 1.0)
print('Generated: ', tree.name)

tree = LevC('LevyC')
tree.generate_fractals(16)
print('Generated: ', tree.name)

tree = DragonCurve('DragonCurve')
tree.generate_fractals(14)
print('Generated: ', tree.name)

tree = Hilbert('Hilbert')
tree.generate_fractals(9)
print('Generated: ', tree.name)
